# Raspberry-based Digital Readout - Installation Instructions

## Hardware Setup

### Preconditions

Requires three AS5311-based magnetic sensors. Wiring diagram to be documented. For now see:

https://forum.zerspanungsbude.net/viewtopic.php?f=99&t=23348 

https://forum.zerspanungsbude.net/viewtopic.php?f=13&t=27206

(in German)

## Setting up the Raspberry as a WLAN Access Point

If you want to access the DRO with an external device like a tablet, smartphone, etc., the Raspberry Pi can be configured as a wlan access point. See: https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md

## Installation of the DRO Software

:warning: Please note that the following steps will take some time.

### Preconditions

:warning: The following steps require Raspian Jessie on the Raspberry Pi. Port 80 must be available in the host system.

### Step 1

Make sure curl is installed:
```bash
sudo apt update && sudo apt -y upgrade && sudo apt install curl
```

### Step 2

Install the latest DRO release (Docker-based) by executing the following script:
```bash
curl -sSL https://gitlab.com/schmelly/DRO/raw/master/setup/install_master.sh | sh
```
This will install Docker, pull the latest DRO Docker image (master branch, no other releases so far), and automatically run the image as a Docker container.

### Step 3

You can access the DRO via http://%lt;your-raspberry-ip%gt;/ (with an external device),
or via http://localhost/ (with in integrated display)
