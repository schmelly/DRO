/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgReduxModule } from '@angular-redux/store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatOptionModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule
} from '@angular/material';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutesModule } from './app-routes/app-routes.module';
import { AxesComponent } from './axes/axes.component';
import { AxesConfigurationComponent } from './configuration/axes-configuration/axes-configuration.component';
import { AxisComponent } from './axis/axis.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { ContourComponent } from './milling/contour/contour.component';
import { GeneralConfigurationComponent } from './configuration/general-configuration/general-configuration.component';
import { HolesComponent } from './milling/holes/holes.component';
import { MidpointComponent } from './midpoint/midpoint.component';
import { PocketComponent } from './milling/pocket/pocket.component';
import { PointsComponent } from './points/points.component';
import { ShutdownComponent } from './shutdown/shutdown.component';

import { SocketService } from './shared/socket.service';
import { DictValuesPipe } from './dict-values.pipe';
import { TurningConfigurationComponent } from './configuration/turning-configuration/turning-configuration.component';


@NgModule({
  declarations: [
    AppComponent,
    AxesComponent,
    AxesConfigurationComponent,
    AxisComponent,
    CalculatorComponent,
    ConfigurationComponent,
    ContourComponent,
    GeneralConfigurationComponent,
    HolesComponent,
    MidpointComponent,
    PocketComponent,
    PointsComponent,
    ShutdownComponent,
    TurningConfigurationComponent,
    DictValuesPipe,
  ],
  imports: [
    AppRoutesModule,
    BrowserModule,
    FormsModule,
    NgReduxModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    HttpClientModule
  ],
  providers: [
    SocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIconSet(
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg-defs.svg'));
  }
}
