import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'dictValues',  pure: false })
export class DictValuesPipe implements PipeTransform {
    transform(value: any, args: any[] = null): any {
        return Object.keys(value).map(key => value[key]); // .map(key => value[key]);
    }
}
