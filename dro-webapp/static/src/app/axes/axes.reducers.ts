import { combineReducers } from 'redux';

import { axisReducer } from '../axis/axis.reducers';
import { IAxes, IAxis, IPoint } from '../models/models';
import { POINTS_DELETE, POINTS_SELECTED } from '../points/points.reducers';
import { ABS_POS } from '../shared/socket.service';

export const AXES_REINITIALIZE = 'AXES_REINITIALIZE';

const defaultAxis: IAxis = {
    metadata: {
        name: '',
        label: ''
    },
    configuration: {
        invert: false,
        unit: 'mm',
        reference: 'abs',
    },
    absValue: 0,
    incOffset: 0,
    pointName: undefined,
    pointValue: undefined,
    magIndicator: 0
};

export const INITIAL_AXES_STATE: IAxes = {
    xAxis: {
        ...defaultAxis,
        metadata: { ...defaultAxis.metadata, name: 'xAxis', label: 'X' },
        configuration: { ...defaultAxis.configuration }
    },
    yAxis: {
        ...defaultAxis,
        metadata: { ...defaultAxis.metadata, name: 'yAxis', label: 'Y' },
        configuration: { ...defaultAxis.configuration }
    },
    zAxis: {
        ...defaultAxis,
        metadata: { ...defaultAxis.metadata, name: 'zAxis', label: 'Z' },
        configuration: { ...defaultAxis.configuration }
    }
};

function setAbsPosition(state: IAxes, absPosition): IAxes {
    state.xAxis.absValue = absPosition.data.X;
    state.yAxis.absValue = absPosition.data.Y;
    state.zAxis.absValue = absPosition.data.Z;
    state.xAxis.magIndicator = absPosition.data.magX;
    state.yAxis.magIndicator = absPosition.data.magY;
    state.zAxis.magIndicator = absPosition.data.magZ;
    return state;
}

function pointSelected(state: IAxes, point: IPoint): IAxes {
    state.xAxis.pointName = point.name;
    state.yAxis.pointName = point.name;
    state.zAxis.pointName = point.name;
    state.xAxis.pointValue = point.x;
    state.yAxis.pointValue = point.y;
    state.zAxis.pointValue = point.z;
    state.xAxis.configuration.reference = point.name;
    state.yAxis.configuration.reference = point.name;
    state.zAxis.configuration.reference = point.name;
    return state;
}

function deletePoints(state: IAxes): IAxes {
    state.xAxis.pointName = undefined;
    state.yAxis.pointName = undefined;
    state.zAxis.pointName = undefined;
    state.xAxis.pointValue = undefined;
    state.yAxis.pointValue = undefined;
    state.zAxis.pointValue = undefined;
    state.xAxis.configuration.reference = 'abs';
    state.yAxis.configuration.reference = 'abs';
    state.zAxis.configuration.reference = 'abs';
    return state;
}

export function axesReducer(state: IAxes = INITIAL_AXES_STATE, action): IAxes {

    const stateCopy = { ...state };

    switch (action.type) {
        case AXES_REINITIALIZE:
            return action.axes;
        case ABS_POS:
            return setAbsPosition(stateCopy, action.absPosition);
        case POINTS_SELECTED:
            return pointSelected(stateCopy, action.point);
        case POINTS_DELETE:
            return deletePoints(stateCopy);
    }

    if (undefined !== action.axis) {
        stateCopy[action.axis.metadata.name] = axisReducer(action.axis, action);
    }
    return stateCopy;
}
