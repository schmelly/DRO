/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { combineReducers } from 'redux';

import { axesReducer, INITIAL_AXES_STATE } from './axes/axes.reducers';
import { calculatorReducer, INITIAL_CALCULATOR_STATE } from './calculator/calculator.reducers';
import { contourReducer, INITIAL_CONTOUR_STATE } from './milling/contour/contour.reducers';
import { pocketReducer, INITIAL_POCKET_STATE } from './milling/pocket/pocket.reducers';
import { holesReducer, INITIAL_HOLES_STATE } from './milling/holes/holes.reducers';
import { midpointReducer, IMidpointState, INITIAL_MIDPOINT_STATE } from './midpoint/midpoint.reducers';
import { pointsReducer, INITIAL_POINTS_STATE } from './points/points.reducers';

import { IAppConfiguration, IAppState, IAxes, ICalculator, IMillingContour, IMillingPocket, IMillingHoles } from './models/models';

export const APP_SELECT_DRO_MODE = 'APP_SELECT_DRO_MODE';

export const INITIAL_APP_CONFIGURATION: IAppConfiguration = {
  mode: 'milling'
};

export const INITIAL_APP_STATE: IAppState = {
  configuration: {
    mode: 'milling'
  },
  axes: INITIAL_AXES_STATE,
  calculator: INITIAL_CALCULATOR_STATE,
  midpoint: INITIAL_MIDPOINT_STATE,
  points: INITIAL_POINTS_STATE,
  millingContour: INITIAL_CONTOUR_STATE,
  millingPocket: INITIAL_POCKET_STATE,
  millingHoles: INITIAL_HOLES_STATE
};

export const rootReducer = combineReducers<IAppState>({
  configuration: appReducer,
  axes: axesReducer,
  midpoint: midpointReducer,
  points: pointsReducer,
  calculator: calculatorReducer,
  millingContour: contourReducer,
  millingPocket: pocketReducer,
  millingHoles: holesReducer
});

export function appReducer(state: IAppConfiguration = INITIAL_APP_CONFIGURATION, action): IAppConfiguration {

  const stateCopy = {...state};

  switch (action.type) {
    case APP_SELECT_DRO_MODE:
    stateCopy.mode = action.mode;
  }

  return stateCopy;
}
