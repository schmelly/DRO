import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurningConfigurationComponent } from './turning-configuration.component';

describe('TurningAxesConfigurationComponent', () => {
  let component: TurningConfigurationComponent;
  let fixture: ComponentFixture<TurningConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurningConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurningConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
