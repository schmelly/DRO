import { Component } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { IAxes } from '../../models/models';

@Component({
  selector: 'app-turning-configuration',
  templateUrl: './turning-configuration.component.html',
  styleUrls: ['./turning-configuration.component.css']
})
export class TurningConfigurationComponent {
  
  @select(['axes']) axes$: Observable<IAxes>;

  constructor() { }
}
