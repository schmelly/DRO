/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Component } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { IAppConfiguration } from '../../models/models';
import { GeneralConfigurationActions } from './general-configuration.actions';

@Component({
  selector: 'app-general-configuration',
  providers: [GeneralConfigurationActions],
  templateUrl: './general-configuration.component.html',
  styleUrls: ['./general-configuration.component.css', './general-configuration.component.theme.scss']
})
export class GeneralConfigurationComponent {

  @select(['configuration']) configuration$: Observable<IAppConfiguration>;

  constructor(
    private generalConfgurationActions: GeneralConfigurationActions
  ) { }

  modeSelect(event) {
    this.generalConfgurationActions.modeSelect(event);
  }
}
