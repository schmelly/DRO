import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AxesConfigurationComponent } from './axes-configuration.component';

describe('ConfigurationComponent', () => {
  let component: AxesConfigurationComponent;
  let fixture: ComponentFixture<AxesConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AxesConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AxesConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
