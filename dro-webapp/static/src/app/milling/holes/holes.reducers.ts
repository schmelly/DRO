/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { IMillingHoles } from '../../models/models';

export const HOLES_REINITIALIZE = 'HOLES_REINITIALIZE';

export const INITIAL_HOLES_STATE: IMillingHoles = {};

export function holesReducer(state: IMillingHoles = INITIAL_HOLES_STATE, action): IMillingHoles {

    switch (action.type) {
        case HOLES_REINITIALIZE:
            return action.holes;
    }

    return state;
}
