/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { ContourActions } from './contour.actions';
import { IAxis, IMillingContour, IPoint, IPoints} from '../../models/models';

interface Point2D {
  x: number;
  y: number;
}

@Component({
  selector: 'app-contour',
  providers: [ContourActions],
  styleUrls: ['./contour.component.css'],
  templateUrl: './contour.component.html'
})
export class ContourComponent implements OnInit {

  @select(['millingContour']) millingContour$: Observable<IMillingContour>;
  @select(['points']) points$: Observable<IPoints>;
  @select(['axes', 'xAxis']) xAxis$: Observable<IAxis>;
  @select(['axes', 'yAxis']) yAxis$: Observable<IAxis>;
  @select(['axes', 'zAxis']) zAxis$: Observable<IAxis>;

  millingContour: IMillingContour;
  points: IPoints;
  xAxis: IAxis;
  yAxis: IAxis;
  zAxis: IAxis;

  constructor(
    private contourActions: ContourActions
  ) { }

  private pointNumberFormat = new Intl.NumberFormat(
    'en',
    {
      style: 'decimal',
      minimumFractionDigits: 2,
      maximumFractionDigits: 3
    }
  );

  ngOnInit() {
    this.millingContour$.subscribe(value => this.millingContour = value);
    this.points$.subscribe(value => this.points = value);
    this.xAxis$.subscribe(value => this.xAxis = value);
    this.yAxis$.subscribe(value => this.yAxis = value);
    this.zAxis$.subscribe(value => this.zAxis = value);
  }

  pointInTriangle(target: Point2D, p0: Point2D, p1: Point2D, p2: Point2D): boolean {
    const Area = 0.5 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y);
    const s = 1 / (2 * Area) * (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * target.x + (p0.x - p2.x) * target.y);
    const t = 1 / (2 * Area) * (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * target.x + (p1.x - p0.x) * target.y);
    return (s > 0) && (t > 0) && ((1 - s - t) > 0);
  }

  getContourZClass(distance: number): string {

    if (this.millingContour.roughing && distance <= 0 && -distance <= this.millingContour.configuration.roughingOffset) {
      return 'contour_collision_warning';
    }

    if (distance < 0) {
      return 'contour_collision';
    } else {
      return 'contour_ok';
    }
  }

  getTrianglePoint(
    vertex: Point2D,
    midpoint: Point2D,
    offsetFor: string,
    roughing: boolean,
    millOffset: number,
    roughingOffet: number): Point2D {

    const a = vertex.y - midpoint.y;
    const b = midpoint.x - vertex.x;
    const c = midpoint.x * vertex.y - vertex.x * midpoint.y;

    let pX: number, pY: number;

    switch (offsetFor) {
      case 'x':
        pX = vertex.x - millOffset;
        if (roughing) {
          pX = pX - roughingOffet;
        }
        pY = (c - a * pX) / b;
        break;
      case 'y':
        pY = vertex.y - millOffset;
        if (roughing) {
          pY = pY - roughingOffet;
        }
        pX = (c - b * pY) / a;
        break;
    }
    return { x: pX, y: pY };
  }

  getContourClass(
    target: Point2D,
    vertex1: Point2D,
    vertex2: Point2D,
    midpoint: Point2D,
    offsetFor: string,
    millOffset: number,
    roughingOffet: number): string {

    const trianglePoint1 = this.getTrianglePoint(vertex1, midpoint, offsetFor, false, millOffset, roughingOffet);
    const trianglePoint2 = this.getTrianglePoint(vertex2, midpoint, offsetFor, false, millOffset, roughingOffet);
    const roughingPoint1 = this.getTrianglePoint(vertex1, midpoint, offsetFor, true, millOffset, roughingOffet);
    const roughingPoint2 = this.getTrianglePoint(vertex2, midpoint, offsetFor, true, millOffset, roughingOffet);
    const pointInTriangle = this.pointInTriangle({ x: target.x, y: target.y }, { x: trianglePoint1.x, y: trianglePoint1.y }, { x: trianglePoint2.x, y: trianglePoint2.y }, { x: midpoint.x, y: midpoint.y });
    const roughingPointInTriangle = this.pointInTriangle({ x: target.x, y: target.y }, { x: roughingPoint1.x, y: roughingPoint1.y }, { x: roughingPoint2.x, y: roughingPoint2.y }, { x: midpoint.x, y: midpoint.y });

    if (this.millingContour.roughing && roughingPointInTriangle && !pointInTriangle) {
      return 'contour_collision_warning';
    } else if (pointInTriangle) {
      return 'contour_collision';
    } else {
      return 'contour_ok';
    }
  }

  getTopLeft(): Point2D {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return { x: 0, y: 0 };
    }

    const pX = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
    const pY = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
    return { x: pX, y: pY };
  }

  getTopRight(): Point2D {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return { x: 0, y: 0 };
    }

    const pX = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
    const pY = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
    return { x: pX, y: pY };
  }

  getBottomLeft(): Point2D {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return { x: 0, y: 0 };
    }

    const pX = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
    const pY = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
    return { x: pX, y: pY };
  }

  getBottomRight(): Point2D {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return { x: 0, y: 0 };
    }

    const pX = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
    const pY = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
    return { x: pX, y: pY };
  }

  getMidPoint(): Point2D {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return { x: 0, y: 0 };
    }

    const mX = (this.millingContour.p1.x + this.millingContour.p2.x) / 2;
    const mY = (this.millingContour.p1.y + this.millingContour.p2.y) / 2;
    return { x: mX, y: mY };
  }

  getContourTopClass(): string {
    const target: Point2D = { x: this.xAxis.absValue, y: this.yAxis.absValue };
    return this.getContourClass(target, this.getTopLeft(), this.getTopRight(), this.getMidPoint(), 'y', -this.millingContour.configuration.millRadius, -this.millingContour.configuration.roughingOffset);
  }

  getContourBottomClass(): string {
    const target: Point2D = { x: this.xAxis.absValue, y: this.yAxis.absValue };
    return this.getContourClass(target, this.getBottomLeft(), this.getBottomRight(), this.getMidPoint(), 'y', this.millingContour.configuration.millRadius, this.millingContour.configuration.roughingOffset);
  }

  getContourRightClass(): string {
    const target: Point2D = { x: this.xAxis.absValue, y: this.yAxis.absValue };
    return this.getContourClass(target, this.getBottomRight(), this.getTopRight(), this.getMidPoint(), 'x', -this.millingContour.configuration.millRadius, -this.millingContour.configuration.roughingOffset);
  }

  getContourLeftClass(): string {
    const target: Point2D = { x: this.xAxis.absValue, y: this.yAxis.absValue };
    return this.getContourClass(target, this.getBottomLeft(), this.getTopLeft(), this.getMidPoint(), 'x', this.millingContour.configuration.millRadius, this.millingContour.configuration.roughingOffset);
  }

  getContourBottomZClass(): string {

    return this.getContourZClass(this.getContourDistZ());
  }

  getContourDrillTransform(): string {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return `translate(0,0)`;
    }

    let xMin = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
    let xMax = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
    let yMin = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
    let yMax = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
    let xLocation;
    let yLocation;

    xMin = xMin - this.millingContour.configuration.millRadius;
    xMax = xMax + this.millingContour.configuration.millRadius;
    yMin = yMin - this.millingContour.configuration.millRadius;
    yMax = yMax + this.millingContour.configuration.millRadius;

    if (this.millingContour.roughing) {
      xMin = xMin - this.millingContour.configuration.roughingOffset;
      xMax = xMax + this.millingContour.configuration.roughingOffset;
      yMin = yMin - this.millingContour.configuration.roughingOffset;
      yMax = yMax + this.millingContour.configuration.roughingOffset;
    }

    if ((xMax - xMin) <= 0) {
      xLocation = 0;
    } else {
      xLocation = (this.xAxis.absValue - xMin) / (xMax - xMin);
    }
    xLocation = 50 * xLocation;

    if ((yMax - yMin) <= 0) {
      yLocation = 0;
    } else {
      yLocation = (this.yAxis.absValue - yMin) / (yMax - yMin) * -1;
    }
    yLocation = 50 * yLocation;

    return `translate(${xLocation},${yLocation})`;
  }

  getContourDrillTransformZ(): string {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return `translate(0,0)`;
    }

    let zMax = Math.max(this.millingContour.p1.z, this.millingContour.p2.z);
    let zLocation;

    if (this.millingContour.roughing) {
      zMax = zMax + this.millingContour.configuration.roughingOffset;
    }
    if (zMax === 0) {
      zLocation = 0;
    } else {
      zLocation = this.zAxis.absValue / zMax;
    }
    zLocation = 25 * zLocation;

    return `translate(0,${zLocation})`;
  }

  getDistLabel(distance: number): string {
    return `${this.pointNumberFormat.format(distance)}`;
  }

  getPointDistance(c1: number, c2: number): number {
    let distance = c2 - c1 - this.millingContour.configuration.millRadius;
    if (this.millingContour.roughing) {
      distance = distance - this.millingContour.configuration.roughingOffset;
    }
    return distance;
  }

  getContourDistTop(): number {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
    return this.getPointDistance(relevantValue, this.yAxis.absValue);
  }

  getContourDistBottom(): number {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
    return this.getPointDistance(this.yAxis.absValue, relevantValue);
  }

  getContourDistLeft(): number {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
    return this.getPointDistance(this.xAxis.absValue, relevantValue);
  }

  getContourDistRight(): number {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
    return this.getPointDistance(relevantValue, this.xAxis.absValue);
  }

  getContourDistZ(): number {

    if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
      return 0;
    }

    let relevantValue = Math.max(this.millingContour.p1.z, this.millingContour.p2.z);
    if (this.millingContour.roughing) {
      relevantValue = relevantValue + this.millingContour.configuration.roughingOffset;
    }
    const distance = this.zAxis.absValue - relevantValue;
    return distance;
  }

  getPointOption(point: IPoint): string {
    return `${point.name}: [${this.pointNumberFormat.format(point.x)},${this.pointNumberFormat.format(point.y)},${this.pointNumberFormat.format(point.z)}]`;
  }

  getFinishMode(): string {
    switch (this.millingContour.roughing) {
      case true:
        return 'roughing';
      case false:
        return 'finishing';
    }
  }

  getP1() {
    return this.millingContour.p1;
  }

  getP2() {
    return this.millingContour.p2;
  }

  p1Select(event): void {
    this.contourActions.p1Select(event.value);
  }

  p2Select(event): void {
    this.contourActions.p2Select(event.value);
  }

  radioSelect(event): void {
    this.contourActions.radioSelect(event.value);
  }
}
