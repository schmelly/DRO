/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { IMillingPocket, IPoint } from '../../models/models';

export const POCKET_REINITIALIZE = 'POCKET_REINITIALIZE';
export const POCKET_SET_POINTS = 'POCKET_SET_POINTS';
export const POCKET_SET_FINISHING_MODE = 'POCKET_SET_FINISHING_MODE';

export const INITIAL_POCKET_STATE: IMillingPocket = {
    p1: undefined,
    p2: undefined,
    roughing: true,
    configuration: {
        millRadius: 2.0,
        roughingOffset: 0.25,
    }
};

export function pocketReducer(state: IMillingPocket = INITIAL_POCKET_STATE, action): IMillingPocket {

    const stateCopy: IMillingPocket = {...state};
    switch (action.type) {
        case POCKET_REINITIALIZE:
            return action.pocket;
        case POCKET_SET_POINTS:
            stateCopy.p1 = action.p1;
            stateCopy.p2 = action.p2;
            return stateCopy;
        case POCKET_SET_FINISHING_MODE:
            stateCopy.roughing = action.roughing;
            return stateCopy;
    }

    return state;
}
