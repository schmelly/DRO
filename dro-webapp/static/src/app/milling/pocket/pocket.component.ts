/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { PocketActions } from './pocket.actions';
import { IAxis, IMillingPocket, IPoint, IPoints } from '../../models/models';

@Component({
  selector: 'app-pocket',
  providers: [PocketActions],
  styleUrls: ['./pocket.component.css'],
  templateUrl: './pocket.component.html'
})
export class PocketComponent implements OnInit {

  @select(['millingPocket']) millingPocket$: Observable<IMillingPocket>;
  @select(['points']) points$: Observable<IPoints>;
  @select(['axes', 'xAxis']) xAxis$: Observable<IAxis>;
  @select(['axes', 'yAxis']) yAxis$: Observable<IAxis>;
  @select(['axes', 'zAxis']) zAxis$: Observable<IAxis>;

  millingPocket: IMillingPocket;
  points: IPoints;
  xAxis: IAxis;
  yAxis: IAxis;
  zAxis: IAxis;

  private pointNumberFormat = new Intl.NumberFormat(
    'en',
    {
      style: 'decimal',
      minimumFractionDigits: 2,
      maximumFractionDigits: 3
    }
  );

  constructor(
    private pocketActions: PocketActions
  ) { }

  ngOnInit() {
    this.millingPocket$.subscribe(value => this.millingPocket = value);
    this.points$.subscribe(value => this.points = value);
    this.xAxis$.subscribe(value => this.xAxis = value);
    this.yAxis$.subscribe(value => this.yAxis = value);
    this.zAxis$.subscribe(value => this.zAxis = value);
  }

  getPocketClass(distance: number): string {

    if (this.millingPocket.roughing && distance <= 0 && -distance <= this.millingPocket.configuration.roughingOffset) {
      return 'pocket_collision_warning';
    }

    if (distance < 0) {
      return 'pocket_collision';
    } else {
      return 'pocket_ok';
    }
  }

  getPocketTopClass(): string {

    return this.getPocketClass(this.getPocketDistTop());
  }

  getPocketBottomClass() {

    return this.getPocketClass(this.getPocketDistBottom());
  }

  getPocketRightClass() {

    return this.getPocketClass(this.getPocketDistRight());
  }

  getPocketLeftClass() {

    return this.getPocketClass(this.getPocketDistLeft());
  }

  getPocketBottomZClass() {

    return this.getPocketClass(this.getPocketDistZ());
  }

  getPocketDrillTransform() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return `translate(0,0)`;
    }

    let xMin = Math.min(this.millingPocket.p1.x, this.millingPocket.p2.x);
    let xMax = Math.max(this.millingPocket.p1.x, this.millingPocket.p2.x);
    let yMin = Math.min(this.millingPocket.p1.y, this.millingPocket.p2.y);
    let yMax = Math.max(this.millingPocket.p1.y, this.millingPocket.p2.y);
    let xLocation;
    let yLocation;

    xMin = xMin + this.millingPocket.configuration.millRadius;
    xMax = xMax - this.millingPocket.configuration.millRadius;
    yMin = yMin + this.millingPocket.configuration.millRadius;
    yMax = yMax - this.millingPocket.configuration.millRadius;

    if (this.millingPocket.roughing) {
      xMin = xMin + this.millingPocket.configuration.roughingOffset;
      xMax = xMax - this.millingPocket.configuration.roughingOffset;
      yMin = yMin + this.millingPocket.configuration.roughingOffset;
      yMax = yMax - this.millingPocket.configuration.roughingOffset;
    }

    if ((xMax - xMin) <= 0) {
      xLocation = 0;
    } else {
      xLocation = (this.xAxis.absValue - xMin) / (xMax - xMin);
    }
    xLocation = 70 * xLocation;

    if ((yMax - yMin) <= 0) {
      yLocation = 0;
    } else {
      yLocation = (this.yAxis.absValue - yMin) / (yMax - yMin) * -1;
    }
    yLocation = 70 * yLocation;

    return `translate(${xLocation},${yLocation})`;
  }

  getPocketDrillTransformZ() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return `translate(0,0)`;
    }

    let zMax = Math.max(this.millingPocket.p1.z, this.millingPocket.p2.z);
    let zLocation;

    if (this.millingPocket.roughing) {
      zMax = zMax + this.millingPocket.configuration.roughingOffset;
    }
    if (zMax === 0) {
      zLocation = 0;
    } else {
      zLocation = this.zAxis.absValue / zMax;
    }
    zLocation = 25 * zLocation;

    return `translate(0,${zLocation})`;
  }

  getDistLabel(distance: number) {
    return `${this.pointNumberFormat.format(distance)}`;
  }

  getPointDistance(c1: number, c2: number): number {
    let distance = c2 - c1 - this.millingPocket.configuration.millRadius;
    if (this.millingPocket.roughing) {
      distance = distance - this.millingPocket.configuration.roughingOffset;
    }
    return distance;
  }

  getPocketDistTop() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.max(this.millingPocket.p1.y, this.millingPocket.p2.y);
    return this.getPointDistance(this.yAxis.absValue, relevantValue);
  }

  getPocketDistBottom() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.min(this.millingPocket.p1.y, this.millingPocket.p2.y);
    return this.getPointDistance(relevantValue, this.yAxis.absValue);
  }

  getPocketDistLeft() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.min(this.millingPocket.p1.x, this.millingPocket.p2.x);
    return this.getPointDistance(relevantValue, this.xAxis.absValue);
  }

  getPocketDistRight() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return 0;
    }

    const relevantValue = Math.max(this.millingPocket.p1.x, this.millingPocket.p2.x);
    return this.getPointDistance(this.xAxis.absValue, relevantValue);
  }

  getPocketDistZ() {

    if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
      return 0;
    }

    let relevantValue = Math.max(this.millingPocket.p1.z, this.millingPocket.p2.z);
    if (this.millingPocket.roughing) {
      relevantValue = relevantValue + this.millingPocket.configuration.roughingOffset;
    }
    const distance = this.zAxis.absValue - relevantValue;
    return distance;
  }

  getPointOption(point: IPoint) {
    return `${point.name}: [${this.pointNumberFormat.format(point.x)},${this.pointNumberFormat.format(point.y)},${this.pointNumberFormat.format(point.z)}]`;
  }

  getFinishMode() {
    switch (this.millingPocket.roughing) {
      case true:
        return 'roughing';
      case false:
        return 'finishing';
    }
  }

  getP1() {
    return this.millingPocket.p1;
  }

  getP2() {
    return this.millingPocket.p2;
  }

  p1Select(event): void {
    this.pocketActions.p1Select(event.value);
  }

  p2Select(event): void {
    this.pocketActions.p2Select(event.value);
  }

  radioSelect(event): void {
    this.pocketActions.radioSelect(event.value);
  }
}
