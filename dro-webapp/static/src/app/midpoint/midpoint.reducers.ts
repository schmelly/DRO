/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { IMidpoint } from '../models/models';

export const MIDPOINT_REINITIALIZE = 'MIDPOINT_REINITIALIZE';

export interface IMidpointState {
    midpoint: IMidpoint;
}

export const INITIAL_MIDPOINT_STATE: IMidpointState = {
    midpoint: {}
};

export function midpointReducer(state: IMidpointState = INITIAL_MIDPOINT_STATE, action): IMidpointState {

    switch (action.type) {
        case MIDPOINT_REINITIALIZE:
            return action.midpoint;
    }

    return state;
}
