/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { IAxis } from '../models/models';
import { AxisActions } from './axis.actions';

@Component({
  selector: 'app-axis',
  providers: [AxisActions],
  styleUrls: ['./axis.component.css', './axis.component.theme.scss'],
  templateUrl: './axis.component.html'
})
export class AxisComponent {

  numberFormat = new Intl.NumberFormat(
    'en',
    {
      style: 'decimal',
      maximumFractionDigits: 3,
      minimumFractionDigits: 3,
      useGrouping: false
    }
  );

  @Input() axis: IAxis;

  constructor(private actions: AxisActions) { }

  isUnitSelected(unitLabel: string) {
    if (unitLabel === this.axis.configuration.unit) {
      return true;
    } else {
      return false;
    }
  }

  isReferenceSelected(referenceLabel: string) {
    if (referenceLabel === this.axis.configuration.reference) {
      return true;
    } else if (referenceLabel === 'inc' && this.axis.configuration.reference === this.axis.pointName) {
      return true;
    } else {
      return false;
    }
  }

  getMagLabelColor() {
    switch (this.axis.magIndicator) {
      case 0:
        return 'green';
      case 6:
        return 'darkorange';
      case 7:
        return 'darkred';
      default:
        return 'darkorange';
    }
  }

  getAxisValue() {
    let returnValue: number;
    if (this.axis.configuration.reference === 'abs') {
      returnValue = this.axis.absValue;
    } else if (this.axis.configuration.reference === 'inc') {
      returnValue = this.axis.absValue + this.axis.incOffset;
    } else {
      returnValue = this.axis.absValue - this.axis.pointValue;
    }
    return this.formattedValue(returnValue);
  }

  formattedValue(value: number): string {
    let formattedValue = this.numberFormat.format(value);
    if (formattedValue.length > 8) {
      formattedValue = value.toPrecision(3);
      formattedValue = formattedValue.replace('\+', '\-');
    }
    return formattedValue;
  }

  getReferenceLabel() {
    if (this.axis.configuration.reference === 'inc' || this.axis.configuration.reference === 'abs' || this.axis.pointName === undefined) {
      return 'inc';
    } else {
      return this.axis.pointName;
    }
  }

  unitSelection(): void {
    this.actions.changeAxisUnit(this.axis);
  }

  referenceSelection(): void {
    this.actions.changeReference(this.axis);
  }

  zeroSelection(): void {
    this.actions.setZero(this.axis);
  }

  axisSelection(): void {
    this.actions.setAxis(this.axis);
  }
}
