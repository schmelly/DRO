/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

import * as axes_actions from './axis.actions';
import * as socket_actions from '../shared/socket.actions';
import * as points_actions from '../points/points.actions';
import { IAxis, IPoint } from '../models/models';

export const AXIS_CHANGE_UNIT = 'AXIS_CHANGE_UNIT';
export const AXIS_CHANGE_REFERENCE = 'AXIS_CHANGE_REFERENCE';
export const AXIS_SET_ZERO = 'AXIS_SET_ZERO';
export const AXIS_SET_AXIS = 'AXIS_SET_AXIS';
export const AXIS_INVERT = 'AXIS_INVERT';

function setUnit(state: IAxis, unit: string): IAxis {
  state.configuration.unit = unit;
  return state;
}

function setReference(state: IAxis, reference: string): IAxis {
  state.configuration.reference = reference;
  return state;
}

function setZero(state: IAxis, inc: boolean): IAxis {
  /*if(abs!==undefined && abs===true) {
    state[axis.name].incValue = incValue;
    state[axis.name]['reference'] = 'inc';
  }*/
  if (inc !== undefined && inc === true) {
    state.incOffset = -state.absValue;
  }
  return state;
}

function setAxis(state: IAxis, value: number): IAxis {
  state.incOffset = value;
  return state;
}

export function axisReducer(state: IAxis, action): IAxis {

  const stateCopy = { ...state };

  switch (action.type) {
    case AXIS_CHANGE_UNIT:
      return setUnit(stateCopy, action.unit);
    case AXIS_CHANGE_REFERENCE:
      return setReference(stateCopy, action.reference);
    case AXIS_SET_ZERO:
      return setZero(stateCopy, action.inc);
    case AXIS_SET_AXIS:
      return setAxis(stateCopy, action.incOffset);
    case AXIS_INVERT:
      stateCopy.configuration.invert = action.inverted;
      return stateCopy;
  }
  return stateCopy;
}
