/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { IAppState, IAxis, ICalculator } from '../models/models';
import { SocketService } from '../shared/socket.service';

import { NUMBER_FORMAT } from '../calculator/calculator.actions';
import { CALCULATOR_DISPLAY_STRING } from '../calculator/calculator.reducers';

import { AXIS_CHANGE_UNIT, AXIS_CHANGE_REFERENCE, AXIS_SET_ZERO, AXIS_SET_AXIS, AXIS_INVERT } from './axis.reducers';

@Injectable()
export class AxisActions {
  constructor(private calcRedux: NgRedux<IAppState>, private ngRedux: NgRedux<IAxis>, private socketService: SocketService) { }

  changeAxisUnit(axis: IAxis) {

    let unit: string;
    if (axis.configuration.unit === 'mm') {
      unit = 'inch';
    } else {
      unit = 'mm';
    }

    this.ngRedux.dispatch({ type: AXIS_CHANGE_UNIT, axis: axis, unit: unit });
  }

  changeReference(axis: IAxis) {

    let reference: string;
    if (axis.configuration.reference === 'abs') {
      reference = 'inc';
    } else if (axis.configuration.reference === 'inc' && axis.pointName !== undefined) {
      reference = axis.pointName;
    } else {
      reference = 'abs';
    }

    this.ngRedux.dispatch({ type: AXIS_CHANGE_REFERENCE, axis: axis, reference: reference });
  }

  setZero(axis: IAxis) {

    if (axis.configuration.reference === 'abs') {
      this.socketService.setZero(axis.metadata.label);
    } else if (axis.configuration.reference === 'inc') { // TODO: check duplicate condition
      this.ngRedux.dispatch({ type: AXIS_SET_ZERO, axis: axis , inc: true });
    }
  }

  setAxis(axis: IAxis) {
    const calc = this.calcRedux.getState().calculator;
    let displayString = calc.displayString;

    switch (calc.direction) {
      case 'left':
        if ('abs' === axis.configuration.reference) {
          this.socketService.setAbsPostion(axis.metadata.label, Number(displayString));
        } else if ('inc' === axis.configuration.reference) {
          this.ngRedux.dispatch({ type: AXIS_SET_AXIS, axis: axis , incOffset: Number(displayString) - axis.absValue });
        }
        break;
      case 'right':
        if ('abs' === axis.configuration.reference) {
          displayString = NUMBER_FORMAT.format(axis.absValue);
        } else if ('inc' === axis.configuration.reference) {
          displayString = NUMBER_FORMAT.format(axis.absValue + axis.incOffset);
        }
        this.ngRedux.dispatch({ type: CALCULATOR_DISPLAY_STRING, axis: axis , displayString: displayString });
    }
  }

  invertAxisSelectionClick(axis: IAxis) {

    this.ngRedux.dispatch({ type: AXIS_INVERT, axis: axis , inverted: !axis.configuration.invert });
  }
}
