/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import {
  POINTS_DELETE,
  POINTS_REINITIALIZE,
  POINTS_LOAD,
  POINTS_SELECTED
} from './points.reducers';
import { IAppState, IPoint, IPoints } from '../models/models';

@Injectable()
export class PointsActions {

  constructor(private ngRedux: NgRedux<IAppState>) { }

  loadPointsClick(filelist: FileList) {

    const ngRedux = this.ngRedux;
    const fileReader: FileReader = new FileReader();
    let points: IPoints;
    fileReader.onloadend = function (e) {
      const json = JSON.parse(fileReader.result);
      points = { ...json };
      ngRedux.dispatch({ type: POINTS_LOAD, points: points });
    };

    for (let i = 0; i < filelist.length; i++) {
      const file = filelist[i];
      fileReader.readAsText(file);
    }
  }

  deletePointsClick() {
    this.ngRedux.dispatch({ type: POINTS_DELETE });
  }

  emitPoints(points: IPoints) {
    this.ngRedux.dispatch({ type: POINTS_LOAD, points: points });
  }

  pointSelectedClick(point: IPoint) {
    this.ngRedux.dispatch({ type: POINTS_SELECTED, point: point });
  }
}
