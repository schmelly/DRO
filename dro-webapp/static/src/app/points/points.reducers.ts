/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { IPoint, IPoints } from '../models/models';

export const POINTS_REINITIALIZE = 'POINTS_REINITIALIZE';
export const POINTS_LOAD = 'POINTS_LOAD';
export const POINTS_DELETE = 'POINTS_DELETE';
export const POINTS_SELECTED = 'POINTS_SELECTED';

export const INITIAL_POINTS_STATE: IPoints = {};

export function pointsReducer(state: IPoints = INITIAL_POINTS_STATE, action): IPoints {

    let stateCopy: IPoints = {...state};
    switch (action.type) {
        case POINTS_REINITIALIZE:
            return action.points;
        case POINTS_LOAD:
            stateCopy = action.points;
            return stateCopy;
        case POINTS_DELETE:
            stateCopy = Object.assign({}, state);
            stateCopy = {};
            return stateCopy;
    }

    return state;
}
