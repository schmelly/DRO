/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import io from 'socket.io-client';

import { SocketActions } from './socket.actions';
import { AXES_REINITIALIZE } from '../axes/axes.reducers';
import { CALCULATOR_REINITIALIZE } from '../calculator/calculator.reducers';
import { CONFIGURATION_REINITIALIZE } from '../configuration/configuration.reducers';
import { IAppState, IAxis } from '../models/models';

export const ABS_POS = 'ABS_POS';

@Injectable()
export class SocketService {
    private name: string;
    private host: string = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port;
    private socket: io.Socket;
    private connected = false;
    private socketActions: SocketActions;

    constructor(private ngRedux: NgRedux<IAppState>) { }

    get(actions: SocketActions) {
        const socketUrl = this.host;
        this.socketActions = actions;
        this.socket = io.connect(socketUrl, { transports: ['websocket'] });
        this.socket.on('connect', () => this.connected = true);
        this.socket.on('disconnect', () => this.connected = false);
        this.socket.on('error', (error: string) => {
            console.log(`ERROR: "${error}" (${socketUrl})`);
        });
        this.socket.on('absPos', (data) => { actions.absPosition({ data }); });
        this.socket.on('loadConfiguration', (data) => {
            const axesConfig = data['axes'];
            const calculatorConfig = data['calculator'];
            const configurationConfig = data['configuration'];
            this.ngRedux.dispatch({ type: AXES_REINITIALIZE, axes: axesConfig });
            this.ngRedux.dispatch({ type: CALCULATOR_REINITIALIZE, calculator: calculatorConfig });
            this.ngRedux.dispatch({ type: CONFIGURATION_REINITIALIZE, configuration: configurationConfig });
        });
        return this;
    }

    setZero(axisLabel: string) {
        if (this.connected) {
            this.socket.emit('setZero', { axis: axisLabel });
        } else {
            const curAbsPos = {
                data: {
                    'X': this.ngRedux.getState().axes.xAxis.absValue,
                    'Y': this.ngRedux.getState().axes.yAxis.absValue,
                    'Z': this.ngRedux.getState().axes.zAxis.absValue,
                    'magX': 0,
                    'magY': 0,
                    'magZ': 0
                }
            };
            curAbsPos.data[axisLabel] = 0;
            this.socketActions.absPosition(curAbsPos);
        }
    }

    setAbsPostion(axisLabel: string, absPos: number) {

        if (axisLabel === this.ngRedux.getState().axes.xAxis.metadata.label && this.ngRedux.getState().axes.xAxis.configuration.invert) {
            absPos = -absPos;
        } else if (axisLabel === this.ngRedux.getState().axes.yAxis.metadata.label && this.ngRedux.getState().axes.yAxis.configuration.invert) {
            absPos = -absPos;
        } else if (axisLabel === this.ngRedux.getState().axes.zAxis.metadata.label && this.ngRedux.getState().axes.zAxis.configuration.invert) {
            absPos = -absPos;
        }

        if (this.connected) {
            this.socket.emit('setAbsPosition', { axis: axisLabel, absPos: absPos });
        } else {
            const curAbsPos = {
                data: {
                    'X': this.ngRedux.getState().axes.xAxis.absValue,
                    'Y': this.ngRedux.getState().axes.yAxis.absValue,
                    'Z': this.ngRedux.getState().axes.zAxis.absValue,
                    'magX': 0,
                    'magY': 0,
                    'magZ': 0
                }
            };
            curAbsPos.data[axisLabel] = absPos;
            this.socketActions.absPosition(curAbsPos);
        }
    }

    saveConfiguration(appState: IAppState) {
        if (this.connected) {
            this.socket.emit('saveConfiguration', appState);
        }
    }

    loadConfiguration() {
        if (this.connected) {
            this.socket.emit('loadConfiguration');
        }
    }

    shutdown() {
        if (this.connected) {
            this.socket.emit('shutdown');
        }
    }

    reboot() {
        if (this.connected) {
            this.socket.emit('reboot');
        }
    }
}
