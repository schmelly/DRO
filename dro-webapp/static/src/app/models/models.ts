/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

export interface IAppConfiguration {
    mode: string;
}

export interface IAppState {
    configuration: IAppConfiguration;
    axes: IAxes;
    calculator: ICalculator;
    midpoint: IMidpoint;
    points: IPoints;
    millingContour?: IMillingContour;
    millingPocket?: IMillingPocket;
    millingHoles?: IMillingHoles;
}

export interface IAxes {
    [axes: string]: IAxis;
}

export interface IAxis {
    metadata: IAxisMetadata;
    configuration: IAxisConfiguration;
    absValue: number;
    incOffset: number;
    pointName: string;
    pointValue: number;
    magIndicator: number;
}

export interface IAxisConfiguration {
    invert: boolean;
    unit: string;
    reference: string;
}

export interface IAxisMetadata {
    name: string;
    label: string;
}

export interface ICalculator {
    displayString: string;
    displayValue: number;
    buffer: number;
    memoryValue: number;
    op: IOp;
    direction: string;
}

export interface IMillingContour {
    p1: IPoint;
    p2: IPoint;
    roughing: boolean;
    configuration: IMillingContourConfiguration;
}

export interface IMillingContourConfiguration {
    millRadius: number;
    roughingOffset: number;
}

export interface IMillingHoles {}

export interface IMidpoint {}

export type IOp = (firstOperand: number, secondOperand: number) => number;

export interface IMillingPocket {
    p1: IPoint;
    p2: IPoint;
    roughing: boolean;
    configuration: IMillingPocketConfiguration;
}

export interface IMillingPocketConfiguration {
    millRadius: number;
    roughingOffset: number;
}

export interface IPoint {
    name: string;
    x: number;
    y: number;
    z: number;
}

export interface IPoints {
    [point: string]: IPoint;
}

export interface ITurningAxis extends IAxis {
    configuration: ITurningAxisConfiguration;
}

export interface ITurningAxisConfiguration extends IAxisConfiguration {
    doubleValue: boolean;
    addAxis: IAxis;
}
