/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { CalculatorActions } from './calculator.actions';
import { IAppState, ICalculator } from '../models/models';

@Component({
  selector: 'app-calculator',
  providers: [CalculatorActions],
  styleUrls: ['./calculator.component.css'],
  templateUrl: './calculator.component.html'
})
export class CalculatorComponent {

  @select(['calculator']) calculator$: Observable<ICalculator>;

  constructor(
    private ngRedux: NgRedux<IAppState>,
    private calcActions: CalculatorActions
  ) { }

  numberFormat = new Intl.NumberFormat(
    'en',
    {
      style: 'decimal',
      maximumFractionDigits: 4
    }
  );

  isDisplayStringTooLong(displayString: string) {
    return (displayString.indexOf('.') > -1 && displayString.length > 10) ||
      (displayString.indexOf('.') === -1 && displayString.length > 9)
      ;
  }

  formattedDisplayString(displayString: string): string {

    let returnValue = displayString;

    if (this.isDisplayStringTooLong(displayString)) {

      const parts = displayString.split('.');
      if (parts[0].length > 9) {
        returnValue = Number(returnValue).toPrecision(5);
        returnValue = returnValue.replace('\+', '\-');
      } else {
        returnValue = Number(returnValue).toPrecision(9);
      }
    }
    return returnValue;
  }

  clearClick(event): void {
    this.calcActions.clearClick(event.button);
  }

  decimalClick(event): void {
    this.calcActions.decimalClick(event.button);
  }

  directionClick(event): void {
    this.calcActions.directionClick(event.button);
  }

  evalClick(event): void {
    this.calcActions.evalClick(event.button);
  }

  functionClick(event): void {
    this.calcActions.functionClick(event.button);
  }

  negateClick(event): void {
    this.calcActions.negateClick(event.button);
  }

  numericClick(event): void {
    this.calcActions.numericClick(event.button);
  }

  storeClick(event): void {
    this.calcActions.storeClick(event.button);
  }
}
