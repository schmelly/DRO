webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routes/app-routes.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ROUTE_CONFIG */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__calculator_calculator_component__ = __webpack_require__("../../../../../src/app/calculator/calculator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configuration_configuration_component__ = __webpack_require__("../../../../../src/app/configuration/configuration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__milling_contour_contour_component__ = __webpack_require__("../../../../../src/app/milling/contour/contour.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__milling_holes_holes_component__ = __webpack_require__("../../../../../src/app/milling/holes/holes.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__milling_pocket_pocket_component__ = __webpack_require__("../../../../../src/app/milling/pocket/pocket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__points_points_component__ = __webpack_require__("../../../../../src/app/points/points.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__midpoint_midpoint_component__ = __webpack_require__("../../../../../src/app/midpoint/midpoint.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shutdown_shutdown_component__ = __webpack_require__("../../../../../src/app/shutdown/shutdown.component.ts");
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/









var ROUTE_CONFIG = [
    {
        path: '',
        redirectTo: 'calculator',
        pathMatch: 'full'
    },
    {
        path: 'calculator',
        component: __WEBPACK_IMPORTED_MODULE_1__calculator_calculator_component__["a" /* CalculatorComponent */]
    },
    {
        path: 'configuration',
        component: __WEBPACK_IMPORTED_MODULE_2__configuration_configuration_component__["a" /* ConfigurationComponent */]
    },
    {
        path: 'contour',
        component: __WEBPACK_IMPORTED_MODULE_3__milling_contour_contour_component__["a" /* ContourComponent */]
    },
    {
        path: 'holes',
        component: __WEBPACK_IMPORTED_MODULE_4__milling_holes_holes_component__["a" /* HolesComponent */]
    },
    {
        path: 'pocket',
        component: __WEBPACK_IMPORTED_MODULE_5__milling_pocket_pocket_component__["a" /* PocketComponent */]
    },
    {
        path: 'points',
        component: __WEBPACK_IMPORTED_MODULE_6__points_points_component__["a" /* PointsComponent */]
    },
    {
        path: 'midpoint',
        component: __WEBPACK_IMPORTED_MODULE_7__midpoint_midpoint_component__["a" /* MidpointComponent */]
    },
    {
        path: 'shutdown',
        component: __WEBPACK_IMPORTED_MODULE_8__shutdown_shutdown_component__["a" /* ShutdownComponent */]
    },
    {
        path: '**',
        redirectTo: 'calculator'
    }
];
var AppRoutesModule = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(ROUTE_CONFIG);


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: auto 66.5vh 66.5vh auto ;\n        grid-template-columns: auto 66.5vh 66.5vh auto ;\n    -ms-grid-rows: 100%;\n        grid-template-rows: 100%;\n    height: 100%;\n}\n\napp-display {\n    display: block;\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 2;\n        grid-column: 2 / span 1;\n    height: 100%;\n}\n\nmat-sidenav-container {\n    height: 100vh;\n    width: 100vw;\n    position: fixed;\n    left: 0;\n    top: 0;\n    overflow: hidden;\n}\n\nmat-sidenav-content {\n    height: 100vh;\n    width: 100vw;\n    position: fixed;\n    left: 0;\n    top: 0;\n    overflow: hidden;\n}\n\n.template {\n    display: block;\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 3;\n        grid-column: 3 / span 1;\n    width: 100%;\n    height: 100%;\n}\n\nnav {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 50% 50%;\n        grid-template-columns: 50% 50%;\n    -ms-grid-column-align: center;\n        justify-items: center;\n    width: 30vh;\n    margin: 1vh;\n    grid-gap: 1vh;\n}\n\n.mat-icon {\n    height: 100%;\n    width: 100%;\n}\n\n.mat-button {\n    padding: 0;\n    margin: 0;\n    width: 10vh;\n    height: 10vh;\n    min-width: 0;\n}\n\n#toggleNav {\n    position: fixed;\n    left: 98%;\n    top: 50%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<!--div [ngSwitch]=\"mode\">\n    <app-milling *ngSwitchCase=\"'milling'\"></app-milling>\n</div-->\n\n<mat-sidenav-container>\n    <mat-sidenav-content>\n        <div class=\"container\">\n            <app-display></app-display>\n            <div class=\"template\"><router-outlet></router-outlet></div>\n        </div>\n        <button mat-button id=\"toggleNav\" (click)=\"sidenav.toggle()\"><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n    </mat-sidenav-content>\n    <mat-sidenav #sidenav mode=\"over\" opened=\"false\" position=\"end\">\n        <nav *ngIf=\"(mode$ | async) === 'milling' else elseBlock\" class=\"navbar navbar-default\">\n            <button mat-button (click)=\"loadConfiguration()\"><mat-icon svgIcon=\"button_load\"></mat-icon></button>\n            <button mat-button (click)=\"saveConfiguration()\"><mat-icon svgIcon=\"button_save\"></mat-icon></button>\n            <a mat-button [routerLink]=\"['/calculator']\"><mat-icon svgIcon=\"button_calc\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/midpoint']\"><mat-icon svgIcon=\"button_midpoint\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/pocket']\"><mat-icon svgIcon=\"button_pocket\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/contour']\"><mat-icon svgIcon=\"button_contour\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/holes']\"><mat-icon svgIcon=\"button_holes\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/points']\"><mat-icon svgIcon=\"button_points\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/configuration']\"><mat-icon svgIcon=\"button_config\"></mat-icon></a>\n            <button mat-button (click)=\"fullscreenMode()\" #fullscreen><mat-icon svgIcon=\"button_fullscreen\"></mat-icon></button>\n            <a mat-button [routerLink]=\"['/shutdown']\"><mat-icon svgIcon=\"button_shutdown\"></mat-icon></a>\n        </nav>\n    \n        <ng-template #elseBlock>\n        <nav class=\"navbar navbar-default\">\n            <button mat-button (click)=\"loadConfiguration()\"><mat-icon svgIcon=\"button_load\"></mat-icon></button>\n            <button mat-button (click)=\"saveConfiguration()\"><mat-icon svgIcon=\"button_save\"></mat-icon></button>\n            <a mat-button [routerLink]=\"['/calculator']\"><mat-icon svgIcon=\"button_calc\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/midpoint']\"><mat-icon svgIcon=\"button_midpoint\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/points']\"><mat-icon svgIcon=\"button_points\"></mat-icon></a>\n            <a mat-button [routerLink]=\"['/configuration']\"><mat-icon svgIcon=\"button_config\"></mat-icon></a>\n            <button mat-button (click)=\"fullscreenMode()\" #fullscreen><mat-icon svgIcon=\"button_fullscreen\"></mat-icon></button>\n            <a mat-button [routerLink]=\"['/shutdown']\"><mat-icon svgIcon=\"button_shutdown\"></mat-icon></a>\n        </nav>\n        </ng-template>\n    </mat-sidenav>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.theme.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__("../../../../redux/es/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_thunk__ = __webpack_require__("../../../../redux-thunk/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_thunk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_redux_thunk__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_reducers__ = __webpack_require__("../../../../../src/app/app.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_socket_actions__ = __webpack_require__("../../../../../src/app/shared/socket.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_socket_service__ = __webpack_require__("../../../../../src/app/shared/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_screenfull__ = __webpack_require__("../../../../screenfull/dist/screenfull.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_screenfull___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_screenfull__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_svg4everybody__ = __webpack_require__("../../../../svg4everybody/dist/svg4everybody.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_svg4everybody___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_svg4everybody__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/











var store = Object(__WEBPACK_IMPORTED_MODULE_3_redux__["createStore"])(__WEBPACK_IMPORTED_MODULE_6__app_reducers__["b" /* rootReducer */], __WEBPACK_IMPORTED_MODULE_6__app_reducers__["a" /* INITIAL_APP_STATE */], Object(__WEBPACK_IMPORTED_MODULE_3_redux__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_3_redux__["applyMiddleware"])(__WEBPACK_IMPORTED_MODULE_4_redux_thunk___default.a)));
var AppComponent = /** @class */ (function () {
    function AppComponent(ngRedux, socketActions, socketService, router) {
        this.ngRedux = ngRedux;
        this.socketActions = socketActions;
        this.socketService = socketService;
        this.router = router;
        this.socketService.get(socketActions);
    }
    AppComponent.prototype.ngOnInit = function () {
        __WEBPACK_IMPORTED_MODULE_10_svg4everybody__({
            polyfill: true
        });
        this.ngRedux.provideStore(store);
    };
    AppComponent.prototype.saveConfiguration = function () {
        this.socketService.saveConfiguration(this.ngRedux.getState());
    };
    AppComponent.prototype.loadConfiguration = function () {
        this.socketService.loadConfiguration();
    };
    AppComponent.prototype.fullscreenMode = function () {
        __WEBPACK_IMPORTED_MODULE_9_screenfull__["toggle"]();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['configuration', 'mode']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__["Observable"])
    ], AppComponent.prototype, "mode$", void 0);
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            providers: [__WEBPACK_IMPORTED_MODULE_7__shared_socket_actions__["b" /* SocketActions */]],
            styles: [__webpack_require__("../../../../../src/app/app.component.css"), __webpack_require__("../../../../../src/app/app.component.theme.scss")],
            template: __webpack_require__("../../../../../src/app/app.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"],
            __WEBPACK_IMPORTED_MODULE_7__shared_socket_actions__["b" /* SocketActions */],
            __WEBPACK_IMPORTED_MODULE_8__shared_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routes_app_routes_module__ = __webpack_require__("../../../../../src/app/app-routes/app-routes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__axis_axis_component__ = __webpack_require__("../../../../../src/app/axis/axis.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__configuration_axes_configuration_axes_configuration_component__ = __webpack_require__("../../../../../src/app/configuration/axes-configuration/axes-configuration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__calculator_calculator_component__ = __webpack_require__("../../../../../src/app/calculator/calculator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__configuration_configuration_component__ = __webpack_require__("../../../../../src/app/configuration/configuration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__milling_contour_contour_component__ = __webpack_require__("../../../../../src/app/milling/contour/contour.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__display_display_component__ = __webpack_require__("../../../../../src/app/display/display.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__configuration_general_configuration_general_configuration_component__ = __webpack_require__("../../../../../src/app/configuration/general-configuration/general-configuration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__milling_holes_holes_component__ = __webpack_require__("../../../../../src/app/milling/holes/holes.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__midpoint_midpoint_component__ = __webpack_require__("../../../../../src/app/midpoint/midpoint.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__milling_pocket_pocket_component__ = __webpack_require__("../../../../../src/app/milling/pocket/pocket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__points_points_component__ = __webpack_require__("../../../../../src/app/points/points.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__shutdown_shutdown_component__ = __webpack_require__("../../../../../src/app/shutdown/shutdown.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__shared_socket_service__ = __webpack_require__("../../../../../src/app/shared/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__dict_values_pipe__ = __webpack_require__("../../../../../src/app/dict-values.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__configuration_turning_configuration_turning_configuration_component__ = __webpack_require__("../../../../../src/app/configuration/turning-configuration/turning-configuration.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


























var AppModule = /** @class */ (function () {
    function AppModule(iconRegistry, sanitizer) {
        iconRegistry.addSvgIconSet(sanitizer.bypassSecurityTrustResourceUrl('assets/svg-defs.svg'));
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_10__configuration_axes_configuration_axes_configuration_component__["a" /* AxesConfigurationComponent */],
                __WEBPACK_IMPORTED_MODULE_9__axis_axis_component__["a" /* AxisComponent */],
                __WEBPACK_IMPORTED_MODULE_11__calculator_calculator_component__["a" /* CalculatorComponent */],
                __WEBPACK_IMPORTED_MODULE_12__configuration_configuration_component__["a" /* ConfigurationComponent */],
                __WEBPACK_IMPORTED_MODULE_13__milling_contour_contour_component__["a" /* ContourComponent */],
                __WEBPACK_IMPORTED_MODULE_14__display_display_component__["a" /* DisplayComponent */],
                __WEBPACK_IMPORTED_MODULE_15__configuration_general_configuration_general_configuration_component__["a" /* GeneralConfigurationComponent */],
                __WEBPACK_IMPORTED_MODULE_16__milling_holes_holes_component__["a" /* HolesComponent */],
                __WEBPACK_IMPORTED_MODULE_17__midpoint_midpoint_component__["a" /* MidpointComponent */],
                __WEBPACK_IMPORTED_MODULE_18__milling_pocket_pocket_component__["a" /* PocketComponent */],
                __WEBPACK_IMPORTED_MODULE_19__points_points_component__["a" /* PointsComponent */],
                __WEBPACK_IMPORTED_MODULE_20__shutdown_shutdown_component__["a" /* ShutdownComponent */],
                __WEBPACK_IMPORTED_MODULE_23__configuration_turning_configuration_turning_configuration_component__["a" /* TurningConfigurationComponent */],
                __WEBPACK_IMPORTED_MODULE_22__dict_values_pipe__["a" /* DictValuesPipe */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_8__app_routes_app_routes_module__["a" /* AppRoutesModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_redux_store__["NgReduxModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["b" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["c" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatOptionModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["f" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["g" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["h" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["i" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_21__shared_socket_service__["a" /* SocketService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__angular_material__["d" /* MatIconRegistry */], __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export INITIAL_APP_CONFIGURATION */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_APP_STATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return rootReducer; });
/* unused harmony export appReducer */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux__ = __webpack_require__("../../../../redux/es/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__axis_axis_reducers__ = __webpack_require__("../../../../../src/app/axis/axis.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__calculator_calculator_reducers__ = __webpack_require__("../../../../../src/app/calculator/calculator.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__configuration_general_configuration_general_configuration_actions__ = __webpack_require__("../../../../../src/app/configuration/general-configuration/general-configuration.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__milling_contour_contour_reducers__ = __webpack_require__("../../../../../src/app/milling/contour/contour.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__milling_pocket_pocket_reducers__ = __webpack_require__("../../../../../src/app/milling/pocket/pocket.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__milling_holes_holes_reducers__ = __webpack_require__("../../../../../src/app/milling/holes/holes.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__midpoint_midpoint_reducers__ = __webpack_require__("../../../../../src/app/midpoint/midpoint.reducers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__points_points_reducers__ = __webpack_require__("../../../../../src/app/points/points.reducers.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/









var INITIAL_APP_CONFIGURATION = {
    mode: 'milling'
};
var INITIAL_APP_STATE = {
    configuration: {
        mode: 'milling'
    },
    axes: __WEBPACK_IMPORTED_MODULE_1__axis_axis_reducers__["a" /* INITIAL_AXES_STATE */],
    calculator: __WEBPACK_IMPORTED_MODULE_2__calculator_calculator_reducers__["a" /* INITIAL_CALCULATOR_STATE */],
    midpoint: __WEBPACK_IMPORTED_MODULE_7__midpoint_midpoint_reducers__["a" /* INITIAL_MIDPOINT_STATE */],
    points: __WEBPACK_IMPORTED_MODULE_8__points_points_reducers__["a" /* INITIAL_POINTS_STATE */],
    millingContour: __WEBPACK_IMPORTED_MODULE_4__milling_contour_contour_reducers__["a" /* INITIAL_CONTOUR_STATE */],
    millingPocket: __WEBPACK_IMPORTED_MODULE_5__milling_pocket_pocket_reducers__["a" /* INITIAL_POCKET_STATE */],
    millingHoles: __WEBPACK_IMPORTED_MODULE_6__milling_holes_holes_reducers__["a" /* INITIAL_HOLES_STATE */]
};
var rootReducer = Object(__WEBPACK_IMPORTED_MODULE_0_redux__["combineReducers"])({
    configuration: appReducer,
    axes: __WEBPACK_IMPORTED_MODULE_1__axis_axis_reducers__["b" /* axesReducer */],
    midpoint: __WEBPACK_IMPORTED_MODULE_7__midpoint_midpoint_reducers__["b" /* midpointReducer */],
    points: __WEBPACK_IMPORTED_MODULE_8__points_points_reducers__["b" /* pointsReducer */],
    calculator: __WEBPACK_IMPORTED_MODULE_2__calculator_calculator_reducers__["b" /* calculatorReducer */],
    millingContour: __WEBPACK_IMPORTED_MODULE_4__milling_contour_contour_reducers__["b" /* contourReducer */],
    millingPocket: __WEBPACK_IMPORTED_MODULE_5__milling_pocket_pocket_reducers__["b" /* pocketReducer */],
    millingHoles: __WEBPACK_IMPORTED_MODULE_6__milling_holes_holes_reducers__["b" /* holesReducer */]
});
function appReducer(state, action) {
    if (state === void 0) { state = INITIAL_APP_CONFIGURATION; }
    var stateCopy = __assign({}, state);
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_3__configuration_general_configuration_general_configuration_actions__["b" /* SELECT_DRO_MODE */]:
            stateCopy.mode = action.mode;
    }
    return stateCopy;
}


/***/ }),

/***/ "../../../../../src/app/axis/axis.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return REINITIALIZE_AXES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return CHANGE_UNIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CHANGE_REFERENCE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return SET_ZERO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return SET_AXIS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return INVERT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AxisActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_socket_service__ = __webpack_require__("../../../../../src/app/shared/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__calculator_calculator_actions__ = __webpack_require__("../../../../../src/app/calculator/calculator.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var REINITIALIZE_AXES = 'REINITIALIZE_AXES';
var CHANGE_UNIT = 'CHANGE_UNIT';
var CHANGE_REFERENCE = 'CHANGE_REFERENCE';
var SET_ZERO = 'SET_ZERO';
var SET_AXIS = 'SET_AXIS';
var INVERT = 'INVERT';
var AxisActions = /** @class */ (function () {
    function AxisActions(ngRedux, socketService) {
        this.ngRedux = ngRedux;
        this.socketService = socketService;
    }
    AxisActions.prototype.changeAxisUnit = function (axis) {
        var unit;
        if (axis.configuration.unit === 'mm') {
            unit = 'inch';
        }
        else {
            unit = 'mm';
        }
        this.ngRedux.dispatch({ type: CHANGE_UNIT, axis: axis, unit: unit });
    };
    AxisActions.prototype.changeReference = function (axis) {
        var reference;
        if (axis.configuration.reference === 'abs') {
            reference = 'inc';
        }
        else if (axis.configuration.reference === 'inc' && axis.pointName !== undefined) {
            reference = axis.pointName;
        }
        else {
            reference = 'abs';
        }
        this.ngRedux.dispatch({ type: CHANGE_REFERENCE, axis: axis, reference: reference });
    };
    AxisActions.prototype.setZero = function (axis) {
        if (axis.configuration.reference === 'abs') {
            this.socketService.setZero(axis.metadata.label);
        }
        else if (axis.configuration.reference === 'inc') {
            this.ngRedux.dispatch({ type: SET_ZERO, axis: axis, inc: true });
        }
    };
    AxisActions.prototype.setAxis = function (axis) {
        var calc = this.ngRedux.getState().calculator;
        var displayString = calc.displayString;
        switch (calc.direction) {
            case 'left':
                if ('abs' === axis.configuration.reference) {
                    this.socketService.setAbsPostion(axis.metadata.label, Number(displayString));
                }
                else if ('inc' === axis.configuration.reference) {
                    this.ngRedux.dispatch({ type: SET_AXIS, axis: axis, incOffset: Number(displayString) - axis.absValue });
                }
                break;
            case 'right':
                if ('abs' === axis.configuration.reference) {
                    displayString = __WEBPACK_IMPORTED_MODULE_3__calculator_calculator_actions__["e" /* NUMBER_FORMAT */].format(axis.absValue);
                }
                else if ('inc' === axis.configuration.reference) {
                    displayString = __WEBPACK_IMPORTED_MODULE_3__calculator_calculator_actions__["e" /* NUMBER_FORMAT */].format(axis.absValue + axis.incOffset);
                }
                this.ngRedux.dispatch({ type: __WEBPACK_IMPORTED_MODULE_3__calculator_calculator_actions__["d" /* DISPLAY_STRING */], displayString: displayString });
        }
    };
    AxisActions.prototype.invertAxisSelectionClick = function (axis) {
        this.ngRedux.dispatch({ type: INVERT, axis: axis, inverted: !axis.configuration.invert });
    };
    AxisActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"], __WEBPACK_IMPORTED_MODULE_2__shared_socket_service__["a" /* SocketService */]])
    ], AxisActions);
    return AxisActions;
}());



/***/ }),

/***/ "../../../../../src/app/axis/axis.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 18% 18% 18% 18% 18%;\n        grid-template-columns: 18% 18% 18% 18% 18%;\n    -ms-grid-rows: 45% 45%;\n        grid-template-rows: 45% 45%;\n    grid-gap: 1vh;\n    -ms-flex-line-pack: distribute;\n        align-content: space-around;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -ms-grid-column-align: end;\n        justify-items: end;\n    margin-top: 1vh;\n    margin-bottom: 2vh;\n}\n\n.digit {\n    font-family: digit;\n    font-size: 11vh;\n    text-align: right;\n    -ms-grid-column-span: 5;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 5;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n\n.mag {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    text-align: center;\n    font-size: 4vh;\n}\n\n.unit {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 2;\n        grid-column: 2 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    font-size: 3vh;\n}\n\n.abs {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 3;\n        grid-column: 3 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    font-size: 3vh;\n}\n\n.zero {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 4;\n        grid-column: 4 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    font-size: 4vh;\n}\n\n.axis {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 5;\n        grid-column: 5 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    font-size: 4vh;\n}\n\n.label_container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    height: 10vh;\n    width: 10vh;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -ms-flex-line-pack: center;\n        align-content: center;\n    position: absolute;\n}\n\n.label_container div {\n    margin: auto;\n}\n\n.mat-icon {\n    height: 100%;\n    width: 100%;\n}\n\n.mat-button {\n    padding: 0;\n    margin: 0;\n    width: 10vh;\n    height: 10vh;\n    min-width: 0;\n    line-height: normal;\n}\n\n.mat-button span {\n    margin: 0;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}\n\n.label {\n    width: 10vh;\n    z-index: -1;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/axis/axis.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n  <div class=\"digit background\">8888.888</div>\n  <div class=\"digit\">{{getAxisValue()}}</div>\n  <button class=\"mag\" mat-button disabled><span class=\"label\" [style.background-color]=\"getMagLabelColor()\">Mag</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n  <button class=\"unit\" mat-button (click)=\"unitSelection()\">\n      <div class=\"label_container\">\n          <div [class.selected]=\"isUnitSelected('mm')\">mm</div>\n          <div [class.selected]=\"isUnitSelected('inch')\">inch</div>\n      </div>\n      <mat-icon svgIcon=\"button_split\"></mat-icon>\n    </button>\n  <button class=\"abs\" mat-button (click)=\"referenceSelection()\">\n      <div class=\"label_container\">\n          <div [class.selected]=\"isReferenceSelected('abs')\">abs</div>\n          <div [class.selected]=\"isReferenceSelected('inc')\">{{getReferenceLabel()}}</div>\n      </div>\n      <mat-icon svgIcon=\"button_split\"></mat-icon>\n    </button>\n  <button class=\"zero\" mat-button (click)=\"zeroSelection()\"><span class=\"label\">zero</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n  <button class=\"axis\" mat-button (click)=\"axisSelection()\"><span class=\"label\">{{axis.metadata.label}}</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/axis/axis.component.theme.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/axis/axis.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AxisComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__axis_actions__ = __webpack_require__("../../../../../src/app/axis/axis.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var AxisComponent = /** @class */ (function () {
    function AxisComponent(actions) {
        this.actions = actions;
        this.numberFormat = new Intl.NumberFormat('en', {
            style: 'decimal',
            maximumFractionDigits: 3,
            minimumFractionDigits: 3,
            useGrouping: false
        });
    }
    AxisComponent.prototype.isUnitSelected = function (unitLabel) {
        if (unitLabel === this.axis.configuration.unit) {
            return true;
        }
        else {
            return false;
        }
    };
    AxisComponent.prototype.isReferenceSelected = function (referenceLabel) {
        if (referenceLabel === this.axis.configuration.reference) {
            return true;
        }
        else if (referenceLabel === 'inc' && this.axis.configuration.reference === this.axis.pointName) {
            return true;
        }
        else {
            return false;
        }
    };
    AxisComponent.prototype.getMagLabelColor = function () {
        switch (this.axis.magIndicator) {
            case 0:
                return 'green';
            case 6:
                return 'darkorange';
            case 7:
                return 'darkred';
            default:
                return 'darkorange';
        }
    };
    AxisComponent.prototype.getAxisValue = function () {
        var returnValue;
        if (this.axis.configuration.reference === 'abs') {
            returnValue = this.axis.absValue;
        }
        else if (this.axis.configuration.reference === 'inc') {
            returnValue = this.axis.absValue + this.axis.incOffset;
        }
        else {
            returnValue = this.axis.absValue - this.axis.pointValue;
        }
        return this.formattedValue(returnValue);
    };
    AxisComponent.prototype.formattedValue = function (value) {
        var formattedValue = this.numberFormat.format(value);
        if (formattedValue.length > 8) {
            formattedValue = value.toPrecision(3);
            formattedValue = formattedValue.replace('\+', '\-');
        }
        return formattedValue;
    };
    AxisComponent.prototype.getReferenceLabel = function () {
        if (this.axis.configuration.reference === 'inc' || this.axis.configuration.reference === 'abs' || this.axis.pointName === undefined) {
            return 'inc';
        }
        else {
            return this.axis.pointName;
        }
    };
    AxisComponent.prototype.unitSelection = function () {
        this.actions.changeAxisUnit(this.axis);
    };
    AxisComponent.prototype.referenceSelection = function () {
        this.actions.changeReference(this.axis);
    };
    AxisComponent.prototype.zeroSelection = function () {
        this.actions.setZero(this.axis);
    };
    AxisComponent.prototype.axisSelection = function () {
        this.actions.setAxis(this.axis);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AxisComponent.prototype, "axis", void 0);
    AxisComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-axis',
            providers: [__WEBPACK_IMPORTED_MODULE_1__axis_actions__["a" /* AxisActions */]],
            styles: [__webpack_require__("../../../../../src/app/axis/axis.component.css"), __webpack_require__("../../../../../src/app/axis/axis.component.theme.scss")],
            template: __webpack_require__("../../../../../src/app/axis/axis.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__axis_actions__["a" /* AxisActions */]])
    ], AxisComponent);
    return AxisComponent;
}());



/***/ }),

/***/ "../../../../../src/app/axis/axis.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_AXES_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = axesReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__axis_actions__ = __webpack_require__("../../../../../src/app/axis/axis.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_socket_actions__ = __webpack_require__("../../../../../src/app/shared/socket.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__points_points_actions__ = __webpack_require__("../../../../../src/app/points/points.actions.ts");
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var defaultAxis = {
    metadata: {
        name: '',
        label: ''
    },
    configuration: {
        invert: false,
        unit: 'mm',
        reference: 'abs',
    },
    absValue: 0,
    incOffset: 0,
    pointName: undefined,
    pointValue: undefined,
    magIndicator: 0
};
// TODO refactor to IMillingAxesState
var INITIAL_AXES_STATE = {
    xAxis: __assign({}, defaultAxis, { metadata: __assign({}, defaultAxis.metadata, { name: 'xAxis', label: 'X' }), configuration: __assign({}, defaultAxis.configuration) }),
    yAxis: __assign({}, defaultAxis, { metadata: __assign({}, defaultAxis.metadata, { name: 'yAxis', label: 'Y' }), configuration: __assign({}, defaultAxis.configuration) }),
    zAxis: __assign({}, defaultAxis, { metadata: __assign({}, defaultAxis.metadata, { name: 'zAxis', label: 'Z' }), configuration: __assign({}, defaultAxis.configuration) })
};
function setUnit(state, axis, unit) {
    state[axis.metadata.name].configuration.unit = unit;
    return state;
}
function setReference(state, axis, reference) {
    state[axis.metadata.name].configuration.reference = reference;
    return state;
}
function setZero(state, axis, inc) {
    /*if(abs!==undefined && abs===true) {
      state[axis.name].incValue = incValue;
      state[axis.name]['reference'] = 'inc';
    }*/
    if (inc !== undefined && inc === true) {
        state[axis.metadata.name].incOffset = -state[axis.metadata.name].absValue;
    }
    return state;
}
function setAxis(state, axis, value) {
    state[axis.metadata.name].incOffset = value;
    return state;
}
function setAbsPosition(state, absPosition) {
    state.xAxis.absValue = absPosition.data.X;
    state.yAxis.absValue = absPosition.data.Y;
    state.zAxis.absValue = absPosition.data.Z;
    state.xAxis.magIndicator = absPosition.data.magX;
    state.yAxis.magIndicator = absPosition.data.magY;
    state.zAxis.magIndicator = absPosition.data.magZ;
    return state;
}
function pointSelected(state, point) {
    state.xAxis.pointName = point.name;
    state.yAxis.pointName = point.name;
    state.zAxis.pointName = point.name;
    state.xAxis.pointValue = point.x;
    state.yAxis.pointValue = point.y;
    state.zAxis.pointValue = point.z;
    state.xAxis.configuration.reference = point.name;
    state.yAxis.configuration.reference = point.name;
    state.zAxis.configuration.reference = point.name;
    return state;
}
function deletePoints(state) {
    state.xAxis.pointName = undefined;
    state.yAxis.pointName = undefined;
    state.zAxis.pointName = undefined;
    state.xAxis.pointValue = undefined;
    state.yAxis.pointValue = undefined;
    state.zAxis.pointValue = undefined;
    state.xAxis.configuration.reference = 'abs';
    state.yAxis.configuration.reference = 'abs';
    state.zAxis.configuration.reference = 'abs';
    return state;
}
function axesReducer(state, action) {
    if (state === void 0) { state = INITIAL_AXES_STATE; }
    var stateCopy = __assign({}, state);
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__axis_actions__["e" /* REINITIALIZE_AXES */]:
            return action.axes;
        case __WEBPACK_IMPORTED_MODULE_0__axis_actions__["c" /* CHANGE_UNIT */]:
            return setUnit(stateCopy, action.axis, action.unit);
        case __WEBPACK_IMPORTED_MODULE_0__axis_actions__["b" /* CHANGE_REFERENCE */]:
            return setReference(stateCopy, action.axis, action.reference);
        case __WEBPACK_IMPORTED_MODULE_0__axis_actions__["g" /* SET_ZERO */]:
            return setZero(stateCopy, action.axis, action.inc);
        case __WEBPACK_IMPORTED_MODULE_0__axis_actions__["f" /* SET_AXIS */]:
            return setAxis(stateCopy, action.axis, action.incOffset);
        case __WEBPACK_IMPORTED_MODULE_1__shared_socket_actions__["a" /* ABS_POS */]:
            return setAbsPosition(stateCopy, action.absPosition);
        case __WEBPACK_IMPORTED_MODULE_2__points_points_actions__["c" /* POINT_SELECTED */]:
            return pointSelected(stateCopy, action.point);
        case __WEBPACK_IMPORTED_MODULE_2__points_points_actions__["a" /* DELETE_POINTS */]:
            return deletePoints(stateCopy);
        case __WEBPACK_IMPORTED_MODULE_0__axis_actions__["d" /* INVERT */]:
            stateCopy[action.axis.metadata.name].configuration.invert = action.inverted;
            return stateCopy;
    }
    return stateCopy;
}


/***/ }),

/***/ "../../../../../src/app/calculator/calculator.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return REINITIALIZE_CALCULATOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CALCULATOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return DIRECTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return DISPLAY_STRING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return NUMBER_FORMAT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CalculatorActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_CALCULATOR = 'REINITIALIZE_CALCULATOR';
var CALCULATOR = 'CALCULATOR';
var DIRECTION = 'DIRECTION';
var DISPLAY_STRING = 'DISPLAY_STRING';
var NUMBER_FORMAT = new Intl.NumberFormat('en', {
    style: 'decimal',
    maximumFractionDigits: 4,
    useGrouping: false
});
var CalculatorActions = /** @class */ (function () {
    function CalculatorActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    CalculatorActions.prototype.evaluate = function (calc) {
        calc.displayValue = Number(calc.displayString);
        calc.displayString = String(calc.displayValue);
        if (calc.op !== undefined) {
            calc.displayValue = calc.op(calc.buffer, calc.displayValue);
            calc.buffer = undefined;
            calc.op = undefined;
            calc.displayString = String(calc.displayValue);
        }
    };
    CalculatorActions.prototype.clearClick = function (button) {
        var calc = __assign({}, this.ngRedux.getState().calculator);
        this.evaluate(calc);
        switch (button) {
            case 'CE':
                calc.buffer = undefined;
                calc.displayString = '0';
                calc.displayValue = undefined;
                calc.memoryValue = undefined;
                calc.op = undefined;
                break;
            case 'C':
                calc.displayString = '0';
                calc.displayValue = 0;
                break;
            case 'del':
                if (calc.displayString.length > 0) {
                    calc.displayString = calc.displayString.substring(0, calc.displayString.length - 1);
                }
                if (calc.displayString.length === 0 || calc.displayString === '-') {
                    calc.displayString = '0';
                }
                break;
        }
        this.ngRedux.dispatch({ type: CALCULATOR, calculator: calc });
    };
    CalculatorActions.prototype.decimalClick = function (button) {
        var displayString = this.ngRedux.getState().calculator.displayString;
        if (displayString.indexOf('.') >= 0 || displayString.length === 0) {
            return;
        }
        displayString = displayString + button;
        this.ngRedux.dispatch({ type: DISPLAY_STRING, displayString: displayString });
    };
    CalculatorActions.prototype.directionClick = function (button) {
        var dir = this.ngRedux.getState().calculator.direction;
        switch (dir) {
            case 'left':
                this.ngRedux.dispatch({ type: DIRECTION, direction: 'right' });
                break;
            default:
                this.ngRedux.dispatch({ type: DIRECTION, direction: 'left' });
                break;
        }
    };
    CalculatorActions.prototype.evalClick = function (button) {
        var calc = __assign({}, this.ngRedux.getState().calculator);
        this.evaluate(calc);
        this.ngRedux.dispatch({ type: CALCULATOR, calculator: calc });
    };
    CalculatorActions.prototype.functionClick = function (button) {
        var calc = __assign({}, this.ngRedux.getState().calculator);
        this.evaluate(calc);
        calc.buffer = calc.displayValue;
        switch (button) {
            case '/':
                calc.op = function (a, b) { return a / b; };
                calc.displayString = '0';
                calc.displayValue = 0;
                break;
            case '+':
                calc.op = function (a, b) { return a + b; };
                calc.displayString = '0';
                calc.displayValue = 0;
                break;
            case '-':
                calc.op = function (a, b) { return a - b; };
                calc.displayString = '0';
                calc.displayValue = 0;
                break;
            case '*':
                calc.op = function (a, b) { return a * b; };
                calc.displayString = '0';
                calc.displayValue = 0;
                break;
            case 'sin':
                calc.op = function (a, b) { return Math.sin(a); };
                this.evaluate(calc);
                break;
            case 'cos':
                calc.op = function (a, b) { return Math.cos(a); };
                this.evaluate(calc);
                break;
            case 'tan':
                calc.op = function (a, b) { return Math.tan(a); };
                this.evaluate(calc);
                break;
            case 'sqrt':
                calc.op = function (a, b) { return Math.sqrt(a); };
                this.evaluate(calc);
                break;
        }
        this.ngRedux.dispatch({ type: CALCULATOR, calculator: calc });
    };
    CalculatorActions.prototype.negateClick = function (button) {
        var displayString = this.ngRedux.getState().calculator.displayString;
        if (displayString.startsWith('-')) {
            displayString = displayString.substring(1);
        }
        else {
            displayString = '-' + displayString;
        }
        this.ngRedux.dispatch({ type: DISPLAY_STRING, displayString: displayString });
    };
    CalculatorActions.prototype.numericClick = function (button) {
        var displayString = this.ngRedux.getState().calculator.displayString;
        displayString = displayString + button;
        if (displayString.startsWith('0') && displayString.length > 1) {
            displayString = displayString.substring(1);
        }
        this.ngRedux.dispatch({ type: DISPLAY_STRING, displayString: displayString });
    };
    CalculatorActions.prototype.storeClick = function (button) {
    };
    CalculatorActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], CalculatorActions);
    return CalculatorActions;
}());



/***/ }),

/***/ "../../../../../src/app/calculator/calculator.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n  display: -ms-grid;\n  display: grid;\n  -ms-grid-columns: auto;\n      grid-template-columns: auto;\n  -ms-grid-rows: auto auto;\n      grid-template-rows: auto auto;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  margin-top: 1vh;\n  margin-bottom: 2vh;\n}\n\n.digit {\n  font-family: digit;\n  text-align: right;\n  font-size: 9vh;\n}\n\n.calc {\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 1;\n      grid-column: 1 / span 1;\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 1;\n      grid-row: 1 / span 1;\n}\n\n.panel {\n  display: -ms-grid;\n  display: grid;\n  -ms-grid-columns: auto auto auto auto auto;\n      grid-template-columns: auto auto auto auto auto;\n  -ms-grid-rows: auto auto auto auto auto;\n      grid-template-rows: auto auto auto auto auto;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  grid-gap: 1vh;\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 1;\n      grid-column: 1 / span 1;\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 2;\n      grid-row: 2 / span 1;\n}\n\n[class~=right] {\n  -webkit-transform: scaleX(-1);\n          transform: scaleX(-1);\n}\n\n.c1 {\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 1;\n      grid-column: 1 / span 1;\n}\n\n.c2 {\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 2;\n      grid-column: 2 / span 1;\n}\n\n.c3 {\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 3;\n      grid-column: 3 / span 1;\n}\n\n.c4 {\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 4;\n      grid-column: 4 / span 1;\n}\n\n.c5 {\n  -ms-grid-column-span: 1;\n  -ms-grid-column: 5;\n      grid-column: 5 / span 1;\n}\n\n.r1 {\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 1;\n      grid-row: 1 / span 1;\n}\n\n.r2 {\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 2;\n      grid-row: 2 / span 1;\n}\n\n.r3 {\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 3;\n      grid-row: 3 / span 1;\n}\n\n.r4 {\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 4;\n      grid-row: 4 / span 1;\n}\n\n.r5 {\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 5;\n      grid-row: 5 / span 1;\n}\n\n.r6 {\n  -ms-grid-row-span: 1;\n  -ms-grid-row: 6;\n      grid-row: 6 / span 1;\n}\n\n.mat-icon {\n  height: 100%;\n  width: 100%;\n}\n\n.mat-button {\n  display: table-cell;\n  padding: 0;\n  margin: 0;\n  width: 10vh;\n  height: 10vh;\n  min-width: 0;\n  line-height: normal;\n}\n\n.mat-button span {\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n\n.label {\n  width: 100%;\n  font-size: 4vh;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/calculator/calculator.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n  <div class=\"digit background calc\">888888888</div>\n  <div class=\"digit calc\">{{formattedDisplayString((calculator$ | async).displayString)}}</div>\n  \n  <div class=\"panel\">\n      <button mat-button [class.right]=\"(calculator$ | async).direction==='right'\" (click)=\"directionClick({button: 'dir'})\"><mat-icon svgIcon=\"button_left\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: 'sin'})\"><mat-icon svgIcon=\"button_sin\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: 'cos'})\"><mat-icon svgIcon=\"button_cos\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: 'tan'})\"><mat-icon svgIcon=\"button_tan\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: 'sqrt'})\"><mat-icon svgIcon=\"button_sqrt\"></mat-icon></button>\n      <button mat-button (click)=\"clearClick({button: 'CE'})\"><span class=\"label\">CE</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"clearClick({button: 'C'})\"><span class=\"label\">C</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"clearClick({button: 'del'})\"><mat-icon svgIcon=\"button_del\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: '/'})\"><span class=\"label\">÷</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"storeClick({button: 'MC'})\"><span class=\"label\">MC</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '7'})\"><span class=\"label\">7</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '8'})\"><span class=\"label\">8</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '9'})\"><span class=\"label\">9</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: '*'})\"><span class=\"label\">x</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"storeClick({button: 'MR'})\"><span class=\"label\">MR</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '4'})\"><span class=\"label\">4</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '5'})\"><span class=\"label\">5</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '6'})\"><span class=\"label\">6</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: '-'})\"><span class=\"label\">-</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"storeClick({button: 'M'})\"><span class=\"label\">M+</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '1'})\"><span class=\"label\">1</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '2'})\"><span class=\"label\">2</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '3'})\"><span class=\"label\">3</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"functionClick({button: '+'})\"><span class=\"label\">+</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"storeClick({button: 'M-'})\"><span class=\"label\">M-</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"negateClick({button: 'neg'})\"><span class=\"label\">±</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"numericClick({button: '0'})\"><span class=\"label\">0</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"decimalClick({button: '.'})\"><span class=\"label\">.</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"evalClick({button: '='})\"><span class=\"label\">=</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n      <button mat-button (click)=\"storeClick({button: 'Ms'})\"><span class=\"label\">Ms</span><mat-icon svgIcon=\"button_empty\"></mat-icon></button>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/calculator/calculator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalculatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__calculator_actions__ = __webpack_require__("../../../../../src/app/calculator/calculator.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var CalculatorComponent = /** @class */ (function () {
    function CalculatorComponent(ngRedux, calcActions) {
        this.ngRedux = ngRedux;
        this.calcActions = calcActions;
        this.numberFormat = new Intl.NumberFormat('en', {
            style: 'decimal',
            maximumFractionDigits: 4
        });
    }
    CalculatorComponent.prototype.isDisplayStringTooLong = function (displayString) {
        return (displayString.indexOf('.') > -1 && displayString.length > 10) ||
            (displayString.indexOf('.') === -1 && displayString.length > 9);
    };
    CalculatorComponent.prototype.formattedDisplayString = function (displayString) {
        var returnValue = displayString;
        if (this.isDisplayStringTooLong(displayString)) {
            var parts = displayString.split('.');
            if (parts[0].length > 9) {
                returnValue = Number(returnValue).toPrecision(5);
                returnValue = returnValue.replace('\+', '\-');
            }
            else {
                returnValue = Number(returnValue).toPrecision(9);
            }
        }
        return returnValue;
    };
    CalculatorComponent.prototype.clearClick = function (event) {
        this.calcActions.clearClick(event.button);
    };
    CalculatorComponent.prototype.decimalClick = function (event) {
        this.calcActions.decimalClick(event.button);
    };
    CalculatorComponent.prototype.directionClick = function (event) {
        this.calcActions.directionClick(event.button);
    };
    CalculatorComponent.prototype.evalClick = function (event) {
        this.calcActions.evalClick(event.button);
    };
    CalculatorComponent.prototype.functionClick = function (event) {
        this.calcActions.functionClick(event.button);
    };
    CalculatorComponent.prototype.negateClick = function (event) {
        this.calcActions.negateClick(event.button);
    };
    CalculatorComponent.prototype.numericClick = function (event) {
        this.calcActions.numericClick(event.button);
    };
    CalculatorComponent.prototype.storeClick = function (event) {
        this.calcActions.storeClick(event.button);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['calculator']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], CalculatorComponent.prototype, "calculator$", void 0);
    CalculatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-calculator',
            providers: [__WEBPACK_IMPORTED_MODULE_3__calculator_actions__["b" /* CalculatorActions */]],
            styles: [__webpack_require__("../../../../../src/app/calculator/calculator.component.css")],
            template: __webpack_require__("../../../../../src/app/calculator/calculator.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"],
            __WEBPACK_IMPORTED_MODULE_3__calculator_actions__["b" /* CalculatorActions */]])
    ], CalculatorComponent);
    return CalculatorComponent;
}());



/***/ }),

/***/ "../../../../../src/app/calculator/calculator.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_CALCULATOR_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = calculatorReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__calculator_actions__ = __webpack_require__("../../../../../src/app/calculator/calculator.actions.ts");
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var INITIAL_CALCULATOR_STATE = {
    displayString: '0',
    displayValue: undefined,
    buffer: undefined,
    memoryValue: undefined,
    op: undefined,
    direction: 'left'
};
var numericRegExp = new RegExp('^[0-9\.]$');
var functionRegExp = new RegExp('^([\-\+\*/])|(sin)|(cos)|(tan)|(sqrt)$');
var evalRegExp = new RegExp('^[=]$');
var negRegExp = new RegExp('^(neg)$');
var storeRegExp = new RegExp('^(MC)|(MR)|(M+)|(M-)|(Ms)$');
var clearRegExp = new RegExp('^(CE)|(C)|(del)$');
var numberFormat = new Intl.NumberFormat('en', {
    style: 'decimal',
    maximumFractionDigits: 4,
    useGrouping: false
});
function handleNumeric(calc, button) {
    if (button === '.') {
        if (calc.displayString.indexOf('.') >= 0 || calc.displayString.length === 0) {
            return;
        }
    }
    calc.displayString = calc.displayString + button;
    if (calc.displayString.startsWith('0') && calc.displayString.length > 1) {
        calc.displayString = calc.displayString.substring(1);
    }
}
// '^[/\+\-\*]|(sin)|(cos)|(tan)|(sqr)$'
function handleFunction(calc, button) {
    handleEval(calc);
    calc.buffer = calc.displayValue;
    switch (button) {
        case '/':
            calc.op = function (a, b) { return a / b; };
            calc.displayString = '0';
            calc.displayValue = 0;
            break;
        case '+':
            calc.op = function (a, b) { return a + b; };
            calc.displayString = '0';
            calc.displayValue = 0;
            break;
        case '-':
            calc.op = function (a, b) { return a - b; };
            calc.displayString = '0';
            calc.displayValue = 0;
            break;
        case '*':
            calc.op = function (a, b) { return a * b; };
            calc.displayString = '0';
            calc.displayValue = 0;
            break;
        case 'sin':
            calc.op = function (a, b) { return Math.sin(a); };
            handleEval(calc);
            break;
        case 'cos':
            calc.op = function (a, b) { return Math.cos(a); };
            handleEval(calc);
            break;
        case 'tan':
            calc.op = function (a, b) { return Math.tan(a); };
            handleEval(calc);
            break;
        case 'sqrt':
            calc.op = function (a, b) { return Math.sqrt(a); };
            handleEval(calc);
            break;
    }
}
function handleEval(calc) {
    calc.displayValue = Number(calc.displayString);
    calc.displayString = numberFormat.format(calc.displayValue);
    if (calc.op !== undefined) {
        calc.displayValue = calc.op(calc.buffer, calc.displayValue);
        calc.buffer = undefined;
        calc.op = undefined;
        calc.displayString = numberFormat.format(calc.displayValue);
    }
}
function handleNeg(calc) {
    if (calc.displayString.startsWith('-')) {
        calc.displayString = calc.displayString.substring(1);
    }
    else {
        calc.displayString = '-' + calc.displayString;
    }
}
function handleStore(calc, button) {
}
function handleClear(calc, button) {
    switch (button) {
        case 'CE':
            calc.buffer = undefined;
            calc.displayString = '0';
            calc.displayValue = undefined;
            calc.memoryValue = undefined;
            calc.op = undefined;
            break;
        case 'C':
            calc.displayString = '0';
            calc.displayValue = 0;
            break;
        case 'del':
            if (calc.displayString.length > 0) {
                calc.displayString = calc.displayString.substring(0, calc.displayString.length - 1);
            }
            if (calc.displayString.length === 0 || calc.displayString === '-') {
                calc.displayString = '0';
            }
            break;
    }
}
function handleDir(calc) {
    switch (calc.direction) {
        case 'left':
            calc.direction = 'right';
            break;
        default:
            calc.direction = 'left';
            break;
    }
}
function calculatorReducer(state, action) {
    if (state === void 0) { state = INITIAL_CALCULATOR_STATE; }
    var stateCopy = __assign({}, state);
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__calculator_actions__["f" /* REINITIALIZE_CALCULATOR */]:
            return action.calculator;
        case __WEBPACK_IMPORTED_MODULE_0__calculator_actions__["a" /* CALCULATOR */]:
            stateCopy = action.calculator;
            return stateCopy;
        case __WEBPACK_IMPORTED_MODULE_0__calculator_actions__["c" /* DIRECTION */]:
            var calc = stateCopy;
            calc.direction = action.direction;
            return stateCopy;
        case __WEBPACK_IMPORTED_MODULE_0__calculator_actions__["d" /* DISPLAY_STRING */]:
            calc = stateCopy;
            calc.displayString = action.displayString;
            return stateCopy;
    }
    return state;
}


/***/ }),

/***/ "../../../../../src/app/configuration/axes-configuration/axes-configuration.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n/*.container {\n    display: flex;\n    justify-content: space-around;\n    flex-direction: column;\n    font-size: 3vh;\n}*/\n\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: auto;\n        grid-template-columns: auto;\n    margin-top: 1vh;\n    margin-bottom: 2vh;\n    grid-gap: 1vh;\n}\n\n.container h1 {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/configuration/axes-configuration/axes-configuration.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<mat-card class=\"container\">\n    <h1 class=\"mat-h1\">Invert Axes</h1>\n    <mat-slide-toggle class=\"label\" labelPosition=\"before\" *ngFor=\"let axis of (axes$ | async) | dictValues\" [checked]=\"axis.configuration.invert === true\" (change)=\"invertAxisSelection({axis: axis})\">Invert {{axis.metadata.label}}</mat-slide-toggle>\n</mat-card>\n"

/***/ }),

/***/ "../../../../../src/app/configuration/axes-configuration/axes-configuration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AxesConfigurationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__axis_axis_actions__ = __webpack_require__("../../../../../src/app/axis/axis.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var AxesConfigurationComponent = /** @class */ (function () {
    function AxesConfigurationComponent(axisAction) {
        this.axisAction = axisAction;
    }
    AxesConfigurationComponent.prototype.invertAxisSelection = function (event) {
        this.axisAction.invertAxisSelectionClick(event.axis);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], AxesConfigurationComponent.prototype, "axes$", void 0);
    AxesConfigurationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-axes-configuration',
            providers: [__WEBPACK_IMPORTED_MODULE_3__axis_axis_actions__["a" /* AxisActions */]],
            styles: [__webpack_require__("../../../../../src/app/configuration/axes-configuration/axes-configuration.component.css")],
            template: __webpack_require__("../../../../../src/app/configuration/axes-configuration/axes-configuration.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__axis_axis_actions__["a" /* AxisActions */]])
    ], AxesConfigurationComponent);
    return AxesConfigurationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/configuration/configuration.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return REINITIALIZE_CONFIGURATION; });
/* unused harmony export ConfigurationActions */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_CONFIGURATION = 'REINITIALIZE_CONFIGURATION';
var ConfigurationActions = /** @class */ (function () {
    function ConfigurationActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    ConfigurationActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], ConfigurationActions);
    return ConfigurationActions;
}());



/***/ }),

/***/ "../../../../../src/app/configuration/configuration.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: auto;\n        grid-template-columns: auto;\n    height: 100%;\n    overflow-y: scroll;\n    margin: 1vh;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/configuration/configuration.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n    <app-general-configuration></app-general-configuration>\n    <app-axes-configuration></app-axes-configuration>\n    <app-turning-configuration *ngIf=\"(mode$ | async) === 'turning'\"></app-turning-configuration>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/configuration/configuration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigurationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/



var ConfigurationComponent = /** @class */ (function () {
    function ConfigurationComponent() {
    }
    ConfigurationComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['configuration', 'mode']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ConfigurationComponent.prototype, "mode$", void 0);
    ConfigurationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-configuration',
            template: __webpack_require__("../../../../../src/app/configuration/configuration.component.html"),
            styles: [__webpack_require__("../../../../../src/app/configuration/configuration.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ConfigurationComponent);
    return ConfigurationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/configuration/general-configuration/general-configuration.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SELECT_DRO_MODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralConfigurationActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var SELECT_DRO_MODE = 'SELECT_DRO_MODE';
var GeneralConfigurationActions = /** @class */ (function () {
    function GeneralConfigurationActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    GeneralConfigurationActions.prototype.modeSelect = function (event) {
        this.ngRedux.dispatch({ type: SELECT_DRO_MODE, mode: event.value });
    };
    GeneralConfigurationActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], GeneralConfigurationActions);
    return GeneralConfigurationActions;
}());



/***/ }),

/***/ "../../../../../src/app/configuration/general-configuration/general-configuration.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: auto;\n        grid-template-columns: auto;\n    -ms-grid-rows: auto auto;\n        grid-template-rows: auto auto;\n    grid-gap: 1vh;\n    margin-top: 1vh;\n    margin-bottom: 2vh;\n}\n\n.container h1 {\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n    -ms-grid-column-span: 2;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 2;\n}\n\n.mode_selection {\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    -ms-grid-column-span: 2;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 2;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/configuration/general-configuration/general-configuration.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<mat-card class=\"container\">\n  <h1 class=\"mat-h1\">DRO Settings</h1>\n  <mat-radio-group [ngModel]=\"(configuration$ | async).mode\">\n    <mat-radio-button value=\"milling\" (change)=\"modeSelect($event)\">Milling</mat-radio-button>\n    <mat-radio-button value=\"turning\" (change)=\"modeSelect($event)\">Turning</mat-radio-button>\n  </mat-radio-group>\n</mat-card>\n"

/***/ }),

/***/ "../../../../../src/app/configuration/general-configuration/general-configuration.component.theme.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/configuration/general-configuration/general-configuration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralConfigurationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__general_configuration_actions__ = __webpack_require__("../../../../../src/app/configuration/general-configuration/general-configuration.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var GeneralConfigurationComponent = /** @class */ (function () {
    function GeneralConfigurationComponent(generalConfgurationActions) {
        this.generalConfgurationActions = generalConfgurationActions;
    }
    GeneralConfigurationComponent.prototype.modeSelect = function (event) {
        this.generalConfgurationActions.modeSelect(event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['configuration']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], GeneralConfigurationComponent.prototype, "configuration$", void 0);
    GeneralConfigurationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-general-configuration',
            providers: [__WEBPACK_IMPORTED_MODULE_3__general_configuration_actions__["a" /* GeneralConfigurationActions */]],
            template: __webpack_require__("../../../../../src/app/configuration/general-configuration/general-configuration.component.html"),
            styles: [__webpack_require__("../../../../../src/app/configuration/general-configuration/general-configuration.component.css"), __webpack_require__("../../../../../src/app/configuration/general-configuration/general-configuration.component.theme.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__general_configuration_actions__["a" /* GeneralConfigurationActions */]])
    ], GeneralConfigurationComponent);
    return GeneralConfigurationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/configuration/turning-configuration/turning-configuration.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    grid-gap: 1vh;\n}\n\n.mat-icon {\n    height: 100%;\n    width: 100%;\n}\n\n.mat-button {\n    display: table-cell;\n    padding: 0;\n    margin: 0;\n    width: 10vh;\n    height: 10vh;\n    min-width: 0;\n    line-height: normal;\n}\n\n.mat-button span {\n    margin: 0;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/configuration/turning-configuration/turning-configuration.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<mat-card class=\"container\">\n    <h1 class=\"mat-h1\">Turning</h1>\n    <div class=\"axes-container\" *ngFor=\"let axis of (axes$ | async) | dictValues\">\n        <h2>{{axis.metadata.label}} Axis: </h2>\n        <div><mat-slide-toggle labelPosition=\"before\">Value x2</mat-slide-toggle></div>\n        <mat-form-field class=\"axis_selection\" >\n            <mat-select placeholder=\"Add Axis\" (selectionChange)=\"axisSelect($event)\">\n                <mat-option>None</mat-option>\n                <mat-option *ngFor=\"let axis2 of (axes$ | async) | dictValues\" [value]=\"axis2\">{{axis2.metadata.label}}</mat-option>\n            </mat-select>\n        </mat-form-field>\n    </div>\n</mat-card>\n"

/***/ }),

/***/ "../../../../../src/app/configuration/turning-configuration/turning-configuration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TurningConfigurationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TurningConfigurationComponent = /** @class */ (function () {
    function TurningConfigurationComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], TurningConfigurationComponent.prototype, "axes$", void 0);
    TurningConfigurationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-turning-configuration',
            template: __webpack_require__("../../../../../src/app/configuration/turning-configuration/turning-configuration.component.html"),
            styles: [__webpack_require__("../../../../../src/app/configuration/turning-configuration/turning-configuration.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TurningConfigurationComponent);
    return TurningConfigurationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dict-values.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DictValuesPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DictValuesPipe = /** @class */ (function () {
    function DictValuesPipe() {
    }
    DictValuesPipe.prototype.transform = function (value, args) {
        if (args === void 0) { args = null; }
        return Object.keys(value).map(function (key) { return value[key]; }); // .map(key => value[key]);
    };
    DictValuesPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'dictValues', pure: false })
    ], DictValuesPipe);
    return DictValuesPipe;
}());



/***/ }),

/***/ "../../../../../src/app/display/display.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 100%;\n        grid-template-columns: 100%;\n    -ms-grid-rows: auto auto auto;\n        grid-template-rows: auto auto auto;\n    -webkit-box-pack: space-evenly;\n        -ms-flex-pack: space-evenly;\n            justify-content: space-evenly;\n}\n\n.xAxis {\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n\n.yAxis {\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n}\n\n.zAxis {\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 3;\n        grid-row: 3 / span 1;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/display/display.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n    <app-axis *ngFor=\"let axis of axes$ | async | dictValues\" class=\"{axis.metadata.name}\" \n        [axis]=\"axis\"\n    ></app-axis>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/display/display.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisplayComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/



var DisplayComponent = /** @class */ (function () {
    function DisplayComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], DisplayComponent.prototype, "axes$", void 0);
    DisplayComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-display',
            styles: [__webpack_require__("../../../../../src/app/display/display.component.css")],
            template: __webpack_require__("../../../../../src/app/display/display.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], DisplayComponent);
    return DisplayComponent;
}());



/***/ }),

/***/ "../../../../../src/app/midpoint/midpoint.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return REINITIALIZE_MIDPOINT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MidpointActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_MIDPOINT = 'REINITIALIZE_MIDPOINT';
var MidpointActions = /** @class */ (function () {
    function MidpointActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    MidpointActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], MidpointActions);
    return MidpointActions;
}());



/***/ }),

/***/ "../../../../../src/app/midpoint/midpoint.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/midpoint/midpoint.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n  Midpoint feature to be implemented.\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/midpoint/midpoint.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MidpointComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__midpoint_actions__ = __webpack_require__("../../../../../src/app/midpoint/midpoint.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var MidpointComponent = /** @class */ (function () {
    function MidpointComponent(midpointActions) {
        this.midpointActions = midpointActions;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['midpoint']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], MidpointComponent.prototype, "midpoint$", void 0);
    MidpointComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-midpoint-view',
            providers: [__WEBPACK_IMPORTED_MODULE_3__midpoint_actions__["a" /* MidpointActions */]],
            styles: [__webpack_require__("../../../../../src/app/midpoint/midpoint.component.css")],
            template: __webpack_require__("../../../../../src/app/midpoint/midpoint.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__midpoint_actions__["a" /* MidpointActions */]])
    ], MidpointComponent);
    return MidpointComponent;
}());



/***/ }),

/***/ "../../../../../src/app/midpoint/midpoint.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_MIDPOINT_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = midpointReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__midpoint_actions__ = __webpack_require__("../../../../../src/app/midpoint/midpoint.actions.ts");
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

var INITIAL_MIDPOINT_STATE = {
    midpoint: {}
};
function midpointReducer(state, action) {
    if (state === void 0) { state = INITIAL_MIDPOINT_STATE; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__midpoint_actions__["b" /* REINITIALIZE_MIDPOINT */]:
            return action.midpoint;
    }
    return state;
}


/***/ }),

/***/ "../../../../../src/app/milling/contour/contour.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return REINITIALIZE_CONTOUR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return SET_CONTOUR_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SET_CONTOUR_FINISHING_MODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContourActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_CONTOUR = 'REINITIALIZE_CONTOUR';
var SET_CONTOUR_POINTS = 'SET_CONTOUR_POINTS';
var SET_CONTOUR_FINISHING_MODE = 'SET_CONTOUR_FINISHING_MODE';
var ContourActions = /** @class */ (function () {
    function ContourActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    ContourActions.prototype.p1Select = function (point) {
        this.ngRedux.dispatch({ type: SET_CONTOUR_POINTS, p1: point, p2: this.ngRedux.getState().millingContour.p2 });
    };
    ContourActions.prototype.p2Select = function (point) {
        this.ngRedux.dispatch({ type: SET_CONTOUR_POINTS, p1: this.ngRedux.getState().millingContour.p1, p2: point });
    };
    ContourActions.prototype.radioSelect = function (value) {
        if (value === 'roughing') {
            this.ngRedux.dispatch({ type: SET_CONTOUR_FINISHING_MODE, roughing: true });
        }
        else {
            this.ngRedux.dispatch({ type: SET_CONTOUR_FINISHING_MODE, roughing: false });
        }
    };
    ContourActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], ContourActions);
    return ContourActions;
}());



/***/ }),

/***/ "../../../../../src/app/milling/contour/contour.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 45% 45%;\n        grid-template-columns: 45% 45%;\n    -ms-grid-rows: 10% 10% 70%;\n        grid-template-rows: 10% 10% 70%;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-line-pack: distribute;\n        align-content: space-around;\n}\n\n.svg_path {\n    stroke-width: 1;\n    stroke: #C6CAED;\n}\n\n.svg_container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 69% 21%;\n        grid-template-columns: 69% 21%;\n    -ms-grid-rows: 90%;\n        grid-template-rows: 90%;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 3;\n        grid-row: 3 / span 1;\n    -ms-grid-column-span: 2;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 2;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-line-pack: distribute;\n        align-content: space-around;\n}\n\n.svg_container div,svg {\n    position: relative;\n    width: 100%;\n    height: 100%;\n}\n\n#svg1 {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n\n#svg2 {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 2;\n        grid-column: 2 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n\n.contour_collision {\n    stroke: red;\n}\n\n.contour_collision_warning {\n    stroke: orange;\n}\n\n.contour_ok {\n    stroke: green;\n}\n\n.point_selection {\n    -webkit-appearance: button;\n    -moz-appearance: button;\n    background: none;\n    border-radius: 1vh;\n    border: .5vh solid #124559;\n    width: 100%;\n    height: 7.5vh;\n    color: #C6CAED;\n    font-size: 4vh;\n}\n\n.point_selection:selection {\n    border:none;\n}\n\n.finish_selection {\n    border-style: none;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    -ms-grid-column-span: 2;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 2;\n}\n\n.finish_selection input {\n    width: 3vh;\n    height: 3vh;\n}\n\n.finish_selection label {\n    color: #C6CAED;\n    font-size: 4vh;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/milling/contour/contour.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n    \n    <mat-form-field>\n        <mat-select [value]=\"getP1()\" (selectionChange)=\"p1Select($event)\">\n            <mat-option>None</mat-option>\n            <mat-option *ngFor=\"let point of points | dictValues\" [value]=\"point\">{{getPointOption(point)}}</mat-option>\n        </mat-select>\n    </mat-form-field>\n    \n    <mat-form-field>\n        <mat-select [value]=\"getP2()\" (selectionChange)=\"p2Select($event)\">\n            <mat-option>None</mat-option>\n            <mat-option *ngFor=\"let point of points | dictValues\" [value]=\"point\">{{getPointOption(point)}}</mat-option>\n        </mat-select>\n    </mat-form-field>\n    \n    <mat-radio-group style=\"grid-column: span 2;\" [value]=\"getFinishMode()\">\n        <mat-radio-button value=\"roughing\" (change)=\"radioSelect($event)\">Roughing</mat-radio-button>\n        <mat-radio-button value=\"finishing\" (change)=\"radioSelect($event)\">Finishing</mat-radio-button>\n    </mat-radio-group>\n\n    <div class=\"svg_container\">\n        <div id=\"svg1\">\n            <svg viewBox=\"0 0 100 100\">\n                <g id=\"contour\">\n                    <path class=\"svg_path\" style=\"stroke-width:1px\" d=\"m70 65-5 5m-30-5-5 5m40-40-5 5m-30-5-5 5\"/>\n                    <path [attr.class]=\"getContourTopClass()\" id=\"contour_border_top\" style=\"stroke-width:1px\" d=\"m65 30-5 5m0-5-5 5m0-5-5 5m0-5-5 5m0-5-5 5m0-5-5 5m-5-5h40\"/>\n                    <path [attr.class]=\"getContourBottomClass()\" id=\"contour_border_bottom\" style=\"stroke-width:1px\" d=\"m40 65-5 5m10-5-5 5m10-5-5 5m10-5-5 5m10-5-5 5m10-5-5 5m-30 0h40\"/>\n                    <path [attr.class]=\"getContourRightClass()\" id=\"contour_border_right\" style=\"stroke-width:1px\" d=\"m65 55 5-5m0-5-5 5m0 10 5-5m0-15-5 5m0 20 5-5m0-25-5 5m5-10v40\"/>\n                    <path [attr.class]=\"getContourLeftClass()\" id=\"contour_border_left\" style=\"stroke-width:1px\" d=\"m30 65 5-5m-5 0 5-5m-5 0 5-5m-5 0 5-5m0-5-5 5m5-10-5 5m0-10v40\"/>\n                    <path style=\"stroke-dasharray:1, 1;stroke-width:.5;fill:none\" d=\"m75 25h-50v50h50z\" class=\"svg_path\"/>\n                    <path [attr.transform]=\"getContourDrillTransform()\" id=\"drill\" class=\"svg_path\" style=\"stroke-width:.5;fill:none\" d=\"m15 75h20m-10-10v20m5-10a5 5 0 0 1 -5 5 5 5 0 0 1 -5 -5 5 5 0 0 1 5 -5 5 5 0 0 1 5 5z\"/>\n                    <text id=\"dist_bottom\" style=\"letter-spacing:0px;font-size:7.5px;word-spacing:0px;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"83.893799\" x=\"50.615967\">{{getDistLabel(getContourDistBottom())}}</text>\n                    <text id=\"dist_top\" style=\"font-size:7.5px;letter-spacing:0px;text-anchor:end;word-spacing:0px;text-align:end;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"21.566406\" x=\"50.615967\">{{getDistLabel(getContourDistTop())}}</text>\n                    <text id=\"dist_left\" style=\"font-size:7.5px;letter-spacing:0px;text-anchor:end;word-spacing:0px;text-align:end;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"48.893799\" x=\"22.391357\">{{getDistLabel(getContourDistLeft())}}</text>\n                    <text id=\"dist_right\" style=\"font-size:7.5px;letter-spacing:0px;text-anchor:end;word-spacing:0px;text-align:end;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"56.566406\" x=\"77.615967\">{{getDistLabel(getContourDistRight())}}</text>\n                    <path class=\"svg_path\" style=\"stroke-width:.5;fill:none\" d=\"m49 33 1-3 1 3zm1-3v-15m0 55-1-3h2zm0 15v-15m-20-20 3-1v2zm-15 0h15m40 0-3-1v2zm15 0h-15\"/>\n                </g>\n            </svg>\n        </div>\n        <div id=\"svg2\">\n            <svg viewBox=\"0 0 30 100\">\n                <g id=\"contour\">\n                    <text id=\"dist_z\" style=\"letter-spacing:0px;font-size:7.5px;word-spacing:0px;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" transform=\"rotate(90)\" class=\"svg_text\" y=\"-19.433594\" x=\"4.6159668\">{{getDistLabel(getContourDistZ())}}</text>\n                    <path [attr.transform]=\"getContourDrillTransformZ()\" id=\"drill\" class=\"svg_path\" style=\"stroke-linecap:round;stroke-width:1px;fill:none\" d=\"m15 30h10m-10 0c0 5 1 6 1 6m2-6s-2 5-2 15m9 5c0-5-1-6-1-6m-2 6s2-5 2-15m1-5c0 10-10 10-10 20h10\"/>\n                    <path style=\"stroke-width:1px\" d=\"m10 50v25m-10-20 5-5m-5 0h10m0 5-10 10m10 0-10.177 10.177\" class=\"svg_path\"/>\n                    <path [attr.class]=\"getContourBottomZClass()\" id=\"contour_border_bottom_z\" style=\"stroke-width:1px\" d=\"m10 75h20m-5 25 5-5m0-10-5 5l-10 10m-5-25-10.177 10.177m30.177-10.177-25 25m15-25-20 20\" class=\"svg_path\"/>\n                    <path style=\"stroke-width:.5;fill:none\" d=\"m27 75v-70m0 70-1 3h2z\" class=\"svg_path\"/>\n                </g>\n            </svg>\n        </div>\n    </div>\n    \n</div>\n"

/***/ }),

/***/ "../../../../../src/app/milling/contour/contour.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContourComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contour_actions__ = __webpack_require__("../../../../../src/app/milling/contour/contour.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var ContourComponent = /** @class */ (function () {
    function ContourComponent(contourActions) {
        this.contourActions = contourActions;
        this.pointNumberFormat = new Intl.NumberFormat('en', {
            style: 'decimal',
            minimumFractionDigits: 2,
            maximumFractionDigits: 3
        });
    }
    ContourComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.millingContour$.subscribe(function (value) { return _this.millingContour = value; });
        this.points$.subscribe(function (value) { return _this.points = value; });
        this.xAxis$.subscribe(function (value) { return _this.xAxis = value; });
        this.yAxis$.subscribe(function (value) { return _this.yAxis = value; });
        this.zAxis$.subscribe(function (value) { return _this.zAxis = value; });
    };
    ContourComponent.prototype.pointInTriangle = function (target, p0, p1, p2) {
        var Area = 0.5 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y);
        var s = 1 / (2 * Area) * (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * target.x + (p0.x - p2.x) * target.y);
        var t = 1 / (2 * Area) * (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * target.x + (p1.x - p0.x) * target.y);
        return (s > 0) && (t > 0) && ((1 - s - t) > 0);
    };
    ContourComponent.prototype.getContourZClass = function (distance) {
        if (this.millingContour.roughing && distance <= 0 && -distance <= this.millingContour.configuration.roughingOffset) {
            return 'contour_collision_warning';
        }
        if (distance < 0) {
            return 'contour_collision';
        }
        else {
            return 'contour_ok';
        }
    };
    ContourComponent.prototype.getTrianglePoint = function (vertex, midpoint, offsetFor, roughing, millOffset, roughingOffet) {
        var a = vertex.y - midpoint.y;
        var b = midpoint.x - vertex.x;
        var c = midpoint.x * vertex.y - vertex.x * midpoint.y;
        var pX, pY;
        switch (offsetFor) {
            case 'x':
                pX = vertex.x - millOffset;
                if (roughing) {
                    pX = pX - roughingOffet;
                }
                pY = (c - a * pX) / b;
                break;
            case 'y':
                pY = vertex.y - millOffset;
                if (roughing) {
                    pY = pY - roughingOffet;
                }
                pX = (c - b * pY) / a;
                break;
        }
        return { x: pX, y: pY };
    };
    ContourComponent.prototype.getContourClass = function (target, vertex1, vertex2, midpoint, offsetFor, millOffset, roughingOffet) {
        var trianglePoint1 = this.getTrianglePoint(vertex1, midpoint, offsetFor, false, millOffset, roughingOffet);
        var trianglePoint2 = this.getTrianglePoint(vertex2, midpoint, offsetFor, false, millOffset, roughingOffet);
        var roughingPoint1 = this.getTrianglePoint(vertex1, midpoint, offsetFor, true, millOffset, roughingOffet);
        var roughingPoint2 = this.getTrianglePoint(vertex2, midpoint, offsetFor, true, millOffset, roughingOffet);
        var pointInTriangle = this.pointInTriangle({ x: target.x, y: target.y }, { x: trianglePoint1.x, y: trianglePoint1.y }, { x: trianglePoint2.x, y: trianglePoint2.y }, { x: midpoint.x, y: midpoint.y });
        var roughingPointInTriangle = this.pointInTriangle({ x: target.x, y: target.y }, { x: roughingPoint1.x, y: roughingPoint1.y }, { x: roughingPoint2.x, y: roughingPoint2.y }, { x: midpoint.x, y: midpoint.y });
        if (this.millingContour.roughing && roughingPointInTriangle && !pointInTriangle) {
            return 'contour_collision_warning';
        }
        else if (pointInTriangle) {
            return 'contour_collision';
        }
        else {
            return 'contour_ok';
        }
    };
    ContourComponent.prototype.getTopLeft = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return { x: 0, y: 0 };
        }
        var pX = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
        var pY = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
        return { x: pX, y: pY };
    };
    ContourComponent.prototype.getTopRight = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return { x: 0, y: 0 };
        }
        var pX = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
        var pY = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
        return { x: pX, y: pY };
    };
    ContourComponent.prototype.getBottomLeft = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return { x: 0, y: 0 };
        }
        var pX = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
        var pY = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
        return { x: pX, y: pY };
    };
    ContourComponent.prototype.getBottomRight = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return { x: 0, y: 0 };
        }
        var pX = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
        var pY = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
        return { x: pX, y: pY };
    };
    ContourComponent.prototype.getMidPoint = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return { x: 0, y: 0 };
        }
        var mX = (this.millingContour.p1.x + this.millingContour.p2.x) / 2;
        var mY = (this.millingContour.p1.y + this.millingContour.p2.y) / 2;
        return { x: mX, y: mY };
    };
    ContourComponent.prototype.getContourTopClass = function () {
        var target = { x: this.xAxis.absValue, y: this.yAxis.absValue };
        return this.getContourClass(target, this.getTopLeft(), this.getTopRight(), this.getMidPoint(), 'y', -this.millingContour.configuration.millRadius, -this.millingContour.configuration.roughingOffset);
    };
    ContourComponent.prototype.getContourBottomClass = function () {
        var target = { x: this.xAxis.absValue, y: this.yAxis.absValue };
        return this.getContourClass(target, this.getBottomLeft(), this.getBottomRight(), this.getMidPoint(), 'y', this.millingContour.configuration.millRadius, this.millingContour.configuration.roughingOffset);
    };
    ContourComponent.prototype.getContourRightClass = function () {
        var target = { x: this.xAxis.absValue, y: this.yAxis.absValue };
        return this.getContourClass(target, this.getBottomRight(), this.getTopRight(), this.getMidPoint(), 'x', -this.millingContour.configuration.millRadius, -this.millingContour.configuration.roughingOffset);
    };
    ContourComponent.prototype.getContourLeftClass = function () {
        var target = { x: this.xAxis.absValue, y: this.yAxis.absValue };
        return this.getContourClass(target, this.getBottomLeft(), this.getTopLeft(), this.getMidPoint(), 'x', this.millingContour.configuration.millRadius, this.millingContour.configuration.roughingOffset);
    };
    ContourComponent.prototype.getContourBottomZClass = function () {
        return this.getContourZClass(this.getContourDistZ());
    };
    ContourComponent.prototype.getContourDrillTransform = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return "translate(0,0)";
        }
        var xMin = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
        var xMax = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
        var yMin = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
        var yMax = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
        var xLocation;
        var yLocation;
        xMin = xMin - this.millingContour.configuration.millRadius;
        xMax = xMax + this.millingContour.configuration.millRadius;
        yMin = yMin - this.millingContour.configuration.millRadius;
        yMax = yMax + this.millingContour.configuration.millRadius;
        if (this.millingContour.roughing) {
            xMin = xMin - this.millingContour.configuration.roughingOffset;
            xMax = xMax + this.millingContour.configuration.roughingOffset;
            yMin = yMin - this.millingContour.configuration.roughingOffset;
            yMax = yMax + this.millingContour.configuration.roughingOffset;
        }
        if ((xMax - xMin) <= 0) {
            xLocation = 0;
        }
        else {
            xLocation = (this.xAxis.absValue - xMin) / (xMax - xMin);
        }
        xLocation = 50 * xLocation;
        if ((yMax - yMin) <= 0) {
            yLocation = 0;
        }
        else {
            yLocation = (this.yAxis.absValue - yMin) / (yMax - yMin) * -1;
        }
        yLocation = 50 * yLocation;
        return "translate(" + xLocation + "," + yLocation + ")";
    };
    ContourComponent.prototype.getContourDrillTransformZ = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return "translate(0,0)";
        }
        var zMax = Math.max(this.millingContour.p1.z, this.millingContour.p2.z);
        var zLocation;
        if (this.millingContour.roughing) {
            zMax = zMax + this.millingContour.configuration.roughingOffset;
        }
        if (zMax === 0) {
            zLocation = 0;
        }
        else {
            zLocation = this.zAxis.absValue / zMax;
        }
        zLocation = 25 * zLocation;
        return "translate(0," + zLocation + ")";
    };
    ContourComponent.prototype.getDistLabel = function (distance) {
        return "" + this.pointNumberFormat.format(distance);
    };
    ContourComponent.prototype.getPointDistance = function (c1, c2) {
        var distance = c2 - c1 - this.millingContour.configuration.millRadius;
        if (this.millingContour.roughing) {
            distance = distance - this.millingContour.configuration.roughingOffset;
        }
        return distance;
    };
    ContourComponent.prototype.getContourDistTop = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.max(this.millingContour.p1.y, this.millingContour.p2.y);
        return this.getPointDistance(relevantValue, this.yAxis.absValue);
    };
    ContourComponent.prototype.getContourDistBottom = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.min(this.millingContour.p1.y, this.millingContour.p2.y);
        return this.getPointDistance(this.yAxis.absValue, relevantValue);
    };
    ContourComponent.prototype.getContourDistLeft = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.min(this.millingContour.p1.x, this.millingContour.p2.x);
        return this.getPointDistance(this.xAxis.absValue, relevantValue);
    };
    ContourComponent.prototype.getContourDistRight = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.max(this.millingContour.p1.x, this.millingContour.p2.x);
        return this.getPointDistance(relevantValue, this.xAxis.absValue);
    };
    ContourComponent.prototype.getContourDistZ = function () {
        if (this.millingContour.p1 === undefined || this.millingContour.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.max(this.millingContour.p1.z, this.millingContour.p2.z);
        if (this.millingContour.roughing) {
            relevantValue = relevantValue + this.millingContour.configuration.roughingOffset;
        }
        var distance = this.zAxis.absValue - relevantValue;
        return distance;
    };
    ContourComponent.prototype.getPointOption = function (point) {
        return point.name + ": [" + this.pointNumberFormat.format(point.x) + "," + this.pointNumberFormat.format(point.y) + "," + this.pointNumberFormat.format(point.z) + "]";
    };
    ContourComponent.prototype.getFinishMode = function () {
        switch (this.millingContour.roughing) {
            case true:
                return 'roughing';
            case false:
                return 'finishing';
        }
    };
    ContourComponent.prototype.getP1 = function () {
        return this.millingContour.p1;
    };
    ContourComponent.prototype.getP2 = function () {
        return this.millingContour.p2;
    };
    ContourComponent.prototype.p1Select = function (event) {
        this.contourActions.p1Select(event.value);
    };
    ContourComponent.prototype.p2Select = function (event) {
        this.contourActions.p2Select(event.value);
    };
    ContourComponent.prototype.radioSelect = function (event) {
        this.contourActions.radioSelect(event.value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['millingContour']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ContourComponent.prototype, "millingContour$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['points']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ContourComponent.prototype, "points$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes', 'xAxis']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ContourComponent.prototype, "xAxis$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes', 'yAxis']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ContourComponent.prototype, "yAxis$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes', 'zAxis']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ContourComponent.prototype, "zAxis$", void 0);
    ContourComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contour',
            providers: [__WEBPACK_IMPORTED_MODULE_3__contour_actions__["a" /* ContourActions */]],
            styles: [__webpack_require__("../../../../../src/app/milling/contour/contour.component.css")],
            template: __webpack_require__("../../../../../src/app/milling/contour/contour.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__contour_actions__["a" /* ContourActions */]])
    ], ContourComponent);
    return ContourComponent;
}());



/***/ }),

/***/ "../../../../../src/app/milling/contour/contour.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_CONTOUR_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = contourReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__contour_actions__ = __webpack_require__("../../../../../src/app/milling/contour/contour.actions.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

var INITIAL_CONTOUR_STATE = {
    p1: undefined,
    p2: undefined,
    roughing: true,
    configuration: {
        roughingOffset: 0.25,
        millRadius: 2.0
    }
};
function contourReducer(state, action) {
    if (state === void 0) { state = INITIAL_CONTOUR_STATE; }
    var stateCopy = __assign({}, state);
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__contour_actions__["b" /* REINITIALIZE_CONTOUR */]:
            return action.contour;
        case __WEBPACK_IMPORTED_MODULE_0__contour_actions__["d" /* SET_CONTOUR_POINTS */]:
            stateCopy.p1 = action.p1;
            stateCopy.p2 = action.p2;
            return stateCopy;
        case __WEBPACK_IMPORTED_MODULE_0__contour_actions__["c" /* SET_CONTOUR_FINISHING_MODE */]:
            stateCopy.roughing = action.roughing;
            return stateCopy;
    }
    return state;
}


/***/ }),

/***/ "../../../../../src/app/milling/holes/holes.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return REINITIALIZE_HOLES; });
/* unused harmony export HolesActions */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_HOLES = 'REINITIALIZE_HOLES';
var HolesActions = /** @class */ (function () {
    function HolesActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    HolesActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], HolesActions);
    return HolesActions;
}());



/***/ }),

/***/ "../../../../../src/app/milling/holes/holes.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/milling/holes/holes.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n  Drill Hole patterns to be implemented.\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/milling/holes/holes.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HolesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/



var HolesComponent = /** @class */ (function () {
    function HolesComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['millingHoles']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], HolesComponent.prototype, "millingHoles$", void 0);
    HolesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-holes-view',
            styles: [__webpack_require__("../../../../../src/app/milling/holes/holes.component.css")],
            template: __webpack_require__("../../../../../src/app/milling/holes/holes.component.html")
        })
    ], HolesComponent);
    return HolesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/milling/holes/holes.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_HOLES_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = holesReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__holes_actions__ = __webpack_require__("../../../../../src/app/milling/holes/holes.actions.ts");
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

var INITIAL_HOLES_STATE = {};
function holesReducer(state, action) {
    if (state === void 0) { state = INITIAL_HOLES_STATE; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__holes_actions__["a" /* REINITIALIZE_HOLES */]:
            return action.holes;
    }
    return state;
}


/***/ }),

/***/ "../../../../../src/app/milling/pocket/pocket.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return REINITIALIZE_POCKET; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return SET_POCKET_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SET_POCKET_FINISHING_MODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PocketActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_POCKET = 'REINITIALIZE_POCKET';
var SET_POCKET_POINTS = 'SET_POCKET_POINTS';
var SET_POCKET_FINISHING_MODE = 'SET_POCKET_FINISHING_MODE';
var PocketActions = /** @class */ (function () {
    function PocketActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    PocketActions.prototype.p1Select = function (point) {
        this.ngRedux.dispatch({ type: SET_POCKET_POINTS, p1: point, p2: this.ngRedux.getState().millingPocket.p2 });
    };
    PocketActions.prototype.p2Select = function (point) {
        this.ngRedux.dispatch({ type: SET_POCKET_POINTS, p1: this.ngRedux.getState().millingPocket.p1, p2: point });
    };
    PocketActions.prototype.radioSelect = function (value) {
        if (value === 'roughing') {
            this.ngRedux.dispatch({ type: SET_POCKET_FINISHING_MODE, roughing: true });
        }
        else {
            this.ngRedux.dispatch({ type: SET_POCKET_FINISHING_MODE, roughing: false });
        }
    };
    PocketActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], PocketActions);
    return PocketActions;
}());



/***/ }),

/***/ "../../../../../src/app/milling/pocket/pocket.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 45% 45%;\n        grid-template-columns: 45% 45%;\n    -ms-grid-rows: 10% 10% 70%;\n        grid-template-rows: 10% 10% 70%;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-line-pack: distribute;\n        align-content: space-around;\n}\n\n.svg_path {\n    stroke-width: 1;\n    stroke: #C6CAED;\n}\n\n.svg_container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 69% 21%;\n        grid-template-columns: 69% 21%;\n    -ms-grid-rows: 90%;\n        grid-template-rows: 90%;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 3;\n        grid-row: 3 / span 1;\n    -ms-grid-column-span: 2;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 2;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -ms-flex-line-pack: distribute;\n        align-content: space-around;\n}\n\n.svg_container div,svg {\n    position: relative;\n    width: 100%;\n    height: 100%;\n}\n\n#svg1 {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n\n#svg2 {\n    -ms-grid-column-span: 1;\n    -ms-grid-column: 2;\n        grid-column: 2 / span 1;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 1;\n        grid-row: 1 / span 1;\n}\n\n.pocket_collision {\n    stroke: red;\n}\n\n.pocket_collision_warning {\n    stroke: orange;\n}\n\n.pocket_ok {\n    stroke: green;\n}\n\n.point_selection {\n    -webkit-appearance: button;\n    -moz-appearance: button;\n    background: none;\n    border-radius: 1vh;\n    border: .5vh solid #124559;\n    width: 100%;\n    height: 7.5vh;\n    color: #C6CAED;\n    font-size: 4vh;\n}\n\n.point_selection:selection {\n    border:none;\n}\n\n.finish_selection {\n    border-style: none;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    -ms-grid-column-span: 2;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 2;\n}\n\n.finish_selection input {\n    width: 3vh;\n    height: 3vh;\n}\n\n.finish_selection label {\n    color: #C6CAED;\n    font-size: 4vh;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/milling/pocket/pocket.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n\n    <mat-form-field>\n        <mat-select [value]=\"getP1()\" (selectionChange)=\"p1Select($event)\">\n            <mat-option>None</mat-option>\n            <mat-option *ngFor=\"let point of points | dictValues\" [value]=\"point\">{{getPointOption(point)}}</mat-option>\n        </mat-select>\n    </mat-form-field>\n\n    <mat-form-field>\n        <mat-select [value]=\"getP2()\" (selectionChange)=\"p2Select($event)\">\n            <mat-option>None</mat-option>\n            <mat-option *ngFor=\"let point of points | dictValues\" [value]=\"point\">{{getPointOption(point)}}</mat-option>\n        </mat-select>\n    </mat-form-field>\n\n    <mat-radio-group style=\"grid-column: span 2;\" [value]=\"getFinishMode()\">\n        <mat-radio-button value=\"roughing\" (change)=\"radioSelect($event)\">Roughing</mat-radio-button>\n        <mat-radio-button value=\"finishing\" (change)=\"radioSelect($event)\">Finishing</mat-radio-button>\n    </mat-radio-group>\n\n    <div class=\"svg_container\">\n        <div id=\"svg1\">\n            <svg viewBox=\"0 0 100 100\">\n                <g id=\"pocket\">\n                    <path [attr.class]=\"getPocketTopClass()\" id=\"pocket_border_top\" d=\"m20.354 0-10 10m-0.354 0h80m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10\" />\n                    <path [attr.class]=\"getPocketBottomClass()\" id=\"pocket_border_bottom\" d=\"m20 90-10 10m80-10h-80m80 0-10 10m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10m0-10-10 10\" class=\"svg_path\"/>\n                    <path [attr.class]=\"getPocketRightClass()\" id=\"pocket_border_right\" d=\"m100 10-10 10m0-10v80m10-70-10 10m10 0-10 10m10 0-10 10m10 0l-10 10m10 0-10 10m10 0-10 10m10 0-10 10\" class=\"svg_path\"/>\n                    <path [attr.class]=\"getPocketLeftClass()\" id=\"pocket_border_left\" d=\"m10.354 10-10 10m9.646-10v80m-10 0 10-10m-10 0 10-10m-10 0 10-10m-10 0 10-10m-10 0 10-10m-10 0 10-10m0-10-10 10\" class=\"svg_path\"/>\n                    <path class=\"svg_path\" style=\"stroke-width:1px\" d=\"m10 89.646-10 10m100-99.646-10 10m-79.646-10-10 10m99.646 80-10 10\"/>\n                    <path class=\"svg_path\" style=\"stroke-dasharray:1, 1;stroke-width:.5;fill:none\" d=\"m85 15h-70v70h70z\"/>\n                    <path [attr.transform]=\"getPocketDrillTransform()\" id=\"drill\" class=\"svg_path\" style=\"stroke-width:.5;fill:none\" d=\"m5 85h20m-10-10v20m5-10a5 5 0 0 1 -5 5 5 5 0 0 1 -5 -5 5 5 0 0 1 5 -5 5 5 0 0 1 5 5z\"/>\n                    <text id=\"dist_bottom\" style=\"letter-spacing:0px;font-size:7.5px;word-spacing:0px;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"83.893799\" x=\"50.615967\">{{getDistLabel(getPocketDistBottom())}}</text>\n                    <text id=\"dist_top\" style=\"font-size:7.5px;letter-spacing:0px;text-anchor:end;word-spacing:0px;text-align:end;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"21.566406\" x=\"50.615967\">{{getDistLabel(getPocketDistTop())}}</text>\n                    <text id=\"dist_left\" style=\"font-size:7.5px;letter-spacing:0px;text-anchor:start;word-spacing:0px;text-align:left;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"48.893799\" x=\"17.615967\">{{getDistLabel(getPocketDistLeft())}}</text>\n                    <text id=\"dist_right\" style=\"font-size:7.5px;letter-spacing:0px;text-anchor:end;word-spacing:0px;text-align:end;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" class=\"svg_text\" y=\"56.566406\" x=\"82.391357\">{{getDistLabel(getPocketDistRight())}}</text>\n                    <path class=\"svg_path\" style=\"stroke-width:.5;fill:none\" d=\"m90 50h-12m12 0 3-1v2zm-80 0h12m-12 0-3-1v2zm40 40v-12m0 12-1 3h2zm0-68v-12m-1-3 1 3 1-3z\"/>\n                </g>\n            </svg>\n        </div>\n\n        <div id=\"svg2\">\n            <svg viewBox=\"0 0 30 100\">\n                <g id=\"pocket\">\n                    <text id=\"dist_z\" style=\"letter-spacing:0px;font-size:7.5px;word-spacing:0px;font-family:sans-serif;line-height:125%\" xml:space=\"preserve\" transform=\"rotate(90)\" class=\"svg_text\" y=\"-24.106201\" x=\"4.6159668\">{{getDistLabel(getPocketDistZ())}}</text>\n                    <path [attr.transform]=\"getPocketDrillTransformZ()\" id=\"drill\" class=\"svg_path\" style=\"stroke-linecap:round;stroke-width:1px;fill:none\" d=\"m10 29.493h10m-10 0c0 5 1 6 1 6m2-6s-2 5-2 15m9 5c0-5-1-6-1-6m-2 6s2-5 2-15m1-5c0 10-10 10-10 20h10\"/>\n                    <path class=\"svg_path\" style=\"stroke-width:1px\" d=\"m25 50v25m-20-25v25m-5-20 5-5m20 0h5m-30 0h5m25 5-5 5m5 5-5 5m-20-10-5 5m5 5-5.1768 5.1768\"/>\n                    <path [attr.class]=\"getPocketBottomZClass()\" id=\"pocket_border_bottom_z\" style=\"stroke-width:1px\" d=\"m5 75h20m0 25 5-5m0-10-5 5l-10 10m-4.823-25.177-10.354 10.354m30.177-10.177-25 25m15-25-20 20\"/>\n                    <path style=\"stroke-width:.5;fill:none\" d=\"m23 75v-70m0 70-1 3h2z\" class=\"svg_path\"/>\n                </g>\n            </svg>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/milling/pocket/pocket.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PocketComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pocket_actions__ = __webpack_require__("../../../../../src/app/milling/pocket/pocket.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var PocketComponent = /** @class */ (function () {
    function PocketComponent(pocketActions) {
        this.pocketActions = pocketActions;
        this.pointNumberFormat = new Intl.NumberFormat('en', {
            style: 'decimal',
            minimumFractionDigits: 2,
            maximumFractionDigits: 3
        });
    }
    PocketComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.millingPocket$.subscribe(function (value) { return _this.millingPocket = value; });
        this.points$.subscribe(function (value) { return _this.points = value; });
        this.xAxis$.subscribe(function (value) { return _this.xAxis = value; });
        this.yAxis$.subscribe(function (value) { return _this.yAxis = value; });
        this.zAxis$.subscribe(function (value) { return _this.zAxis = value; });
    };
    PocketComponent.prototype.getPocketClass = function (distance) {
        if (this.millingPocket.roughing && distance <= 0 && -distance <= this.millingPocket.configuration.roughingOffset) {
            return 'pocket_collision_warning';
        }
        if (distance < 0) {
            return 'pocket_collision';
        }
        else {
            return 'pocket_ok';
        }
    };
    PocketComponent.prototype.getPocketTopClass = function () {
        return this.getPocketClass(this.getPocketDistTop());
    };
    PocketComponent.prototype.getPocketBottomClass = function () {
        return this.getPocketClass(this.getPocketDistBottom());
    };
    PocketComponent.prototype.getPocketRightClass = function () {
        return this.getPocketClass(this.getPocketDistRight());
    };
    PocketComponent.prototype.getPocketLeftClass = function () {
        return this.getPocketClass(this.getPocketDistLeft());
    };
    PocketComponent.prototype.getPocketBottomZClass = function () {
        return this.getPocketClass(this.getPocketDistZ());
    };
    PocketComponent.prototype.getPocketDrillTransform = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return "translate(0,0)";
        }
        var xMin = Math.min(this.millingPocket.p1.x, this.millingPocket.p2.x);
        var xMax = Math.max(this.millingPocket.p1.x, this.millingPocket.p2.x);
        var yMin = Math.min(this.millingPocket.p1.y, this.millingPocket.p2.y);
        var yMax = Math.max(this.millingPocket.p1.y, this.millingPocket.p2.y);
        var xLocation;
        var yLocation;
        xMin = xMin + this.millingPocket.configuration.millRadius;
        xMax = xMax - this.millingPocket.configuration.millRadius;
        yMin = yMin + this.millingPocket.configuration.millRadius;
        yMax = yMax - this.millingPocket.configuration.millRadius;
        if (this.millingPocket.roughing) {
            xMin = xMin + this.millingPocket.configuration.roughingOffset;
            xMax = xMax - this.millingPocket.configuration.roughingOffset;
            yMin = yMin + this.millingPocket.configuration.roughingOffset;
            yMax = yMax - this.millingPocket.configuration.roughingOffset;
        }
        if ((xMax - xMin) <= 0) {
            xLocation = 0;
        }
        else {
            xLocation = (this.xAxis.absValue - xMin) / (xMax - xMin);
        }
        xLocation = 70 * xLocation;
        if ((yMax - yMin) <= 0) {
            yLocation = 0;
        }
        else {
            yLocation = (this.yAxis.absValue - yMin) / (yMax - yMin) * -1;
        }
        yLocation = 70 * yLocation;
        return "translate(" + xLocation + "," + yLocation + ")";
    };
    PocketComponent.prototype.getPocketDrillTransformZ = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return "translate(0,0)";
        }
        var zMax = Math.max(this.millingPocket.p1.z, this.millingPocket.p2.z);
        var zLocation;
        if (this.millingPocket.roughing) {
            zMax = zMax + this.millingPocket.configuration.roughingOffset;
        }
        if (zMax === 0) {
            zLocation = 0;
        }
        else {
            zLocation = this.zAxis.absValue / zMax;
        }
        zLocation = 25 * zLocation;
        return "translate(0," + zLocation + ")";
    };
    PocketComponent.prototype.getDistLabel = function (distance) {
        return "" + this.pointNumberFormat.format(distance);
    };
    PocketComponent.prototype.getPointDistance = function (c1, c2) {
        var distance = c2 - c1 - this.millingPocket.configuration.millRadius;
        if (this.millingPocket.roughing) {
            distance = distance - this.millingPocket.configuration.roughingOffset;
        }
        return distance;
    };
    PocketComponent.prototype.getPocketDistTop = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.max(this.millingPocket.p1.y, this.millingPocket.p2.y);
        return this.getPointDistance(this.yAxis.absValue, relevantValue);
    };
    PocketComponent.prototype.getPocketDistBottom = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.min(this.millingPocket.p1.y, this.millingPocket.p2.y);
        return this.getPointDistance(relevantValue, this.yAxis.absValue);
    };
    PocketComponent.prototype.getPocketDistLeft = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.min(this.millingPocket.p1.x, this.millingPocket.p2.x);
        return this.getPointDistance(relevantValue, this.xAxis.absValue);
    };
    PocketComponent.prototype.getPocketDistRight = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.max(this.millingPocket.p1.x, this.millingPocket.p2.x);
        return this.getPointDistance(this.xAxis.absValue, relevantValue);
    };
    PocketComponent.prototype.getPocketDistZ = function () {
        if (this.millingPocket.p1 === undefined || this.millingPocket.p2 === undefined) {
            return 0;
        }
        var relevantValue = Math.max(this.millingPocket.p1.z, this.millingPocket.p2.z);
        if (this.millingPocket.roughing) {
            relevantValue = relevantValue + this.millingPocket.configuration.roughingOffset;
        }
        var distance = this.zAxis.absValue - relevantValue;
        return distance;
    };
    PocketComponent.prototype.getPointOption = function (point) {
        return point.name + ": [" + this.pointNumberFormat.format(point.x) + "," + this.pointNumberFormat.format(point.y) + "," + this.pointNumberFormat.format(point.z) + "]";
    };
    PocketComponent.prototype.getFinishMode = function () {
        switch (this.millingPocket.roughing) {
            case true:
                return 'roughing';
            case false:
                return 'finishing';
        }
    };
    PocketComponent.prototype.getP1 = function () {
        return this.millingPocket.p1;
    };
    PocketComponent.prototype.getP2 = function () {
        return this.millingPocket.p2;
    };
    PocketComponent.prototype.p1Select = function (event) {
        this.pocketActions.p1Select(event.value);
    };
    PocketComponent.prototype.p2Select = function (event) {
        this.pocketActions.p2Select(event.value);
    };
    PocketComponent.prototype.radioSelect = function (event) {
        this.pocketActions.radioSelect(event.value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['millingPocket']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], PocketComponent.prototype, "millingPocket$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['points']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], PocketComponent.prototype, "points$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes', 'xAxis']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], PocketComponent.prototype, "xAxis$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes', 'yAxis']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], PocketComponent.prototype, "yAxis$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['axes', 'zAxis']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], PocketComponent.prototype, "zAxis$", void 0);
    PocketComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pocket',
            providers: [__WEBPACK_IMPORTED_MODULE_3__pocket_actions__["a" /* PocketActions */]],
            styles: [__webpack_require__("../../../../../src/app/milling/pocket/pocket.component.css")],
            template: __webpack_require__("../../../../../src/app/milling/pocket/pocket.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__pocket_actions__["a" /* PocketActions */]])
    ], PocketComponent);
    return PocketComponent;
}());



/***/ }),

/***/ "../../../../../src/app/milling/pocket/pocket.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_POCKET_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = pocketReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pocket_actions__ = __webpack_require__("../../../../../src/app/milling/pocket/pocket.actions.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

var INITIAL_POCKET_STATE = {
    p1: undefined,
    p2: undefined,
    roughing: true,
    configuration: {
        millRadius: 2.0,
        roughingOffset: 0.25,
    }
};
function pocketReducer(state, action) {
    if (state === void 0) { state = INITIAL_POCKET_STATE; }
    var stateCopy = __assign({}, state);
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__pocket_actions__["b" /* REINITIALIZE_POCKET */]:
            return action.pocket;
        case __WEBPACK_IMPORTED_MODULE_0__pocket_actions__["d" /* SET_POCKET_POINTS */]:
            stateCopy.p1 = action.p1;
            stateCopy.p2 = action.p2;
            return stateCopy;
        case __WEBPACK_IMPORTED_MODULE_0__pocket_actions__["c" /* SET_POCKET_FINISHING_MODE */]:
            stateCopy.roughing = action.roughing;
            return stateCopy;
    }
    return state;
}


/***/ }),

/***/ "../../../../../src/app/points/points.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return REINITIALIZE_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LOAD_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DELETE_POINTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return POINT_SELECTED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return PointsActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var REINITIALIZE_POINTS = 'REINITIALIZE_POINTS';
var LOAD_POINTS = 'LOAD_POINTS';
var DELETE_POINTS = 'DELETE_POINTS';
var POINT_SELECTED = 'POINT_SELECTED';
var PointsActions = /** @class */ (function () {
    function PointsActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    PointsActions.prototype.loadPointsClick = function (filelist) {
        var ngRedux = this.ngRedux;
        var fileReader = new FileReader();
        var points;
        fileReader.onloadend = function (e) {
            var json = JSON.parse(fileReader.result);
            points = __assign({}, json);
            ngRedux.dispatch({ type: LOAD_POINTS, points: points });
        };
        for (var i = 0; i < filelist.length; i++) {
            var file = filelist[i];
            fileReader.readAsText(file);
        }
    };
    PointsActions.prototype.deletePointsClick = function () {
        this.ngRedux.dispatch({ type: DELETE_POINTS });
    };
    PointsActions.prototype.emitPoints = function (points) {
        this.ngRedux.dispatch({ type: LOAD_POINTS, points: points });
    };
    PointsActions.prototype.pointSelectedClick = function (point) {
        this.ngRedux.dispatch({ type: POINT_SELECTED, point: point });
    };
    PointsActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], PointsActions);
    return PointsActions;
}());



/***/ }),

/***/ "../../../../../src/app/points/points.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 20% 20% 50%;\n        grid-template-columns: 20% 20% 50%;\n    -ms-grid-rows: 10% auto;\n        grid-template-rows: 10% auto;\n    grid-gap: 1vh;\n    margin: 1vh;\n    height: 100%;\n}\n\n.list {\n    -ms-grid-column-span: 3;\n    -ms-grid-column: 1;\n        grid-column: 1 / span 3;\n    -ms-grid-row-span: 1;\n    -ms-grid-row: 2;\n        grid-row: 2 / span 1;\n    overflow-y: scroll;\n}\n\n.listitem {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 18% 27% 27% 27%;\n        grid-template-columns: 18% 27% 27% 27%;\n    -ms-grid-rows: auto auto;\n        grid-template-rows: auto auto;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n\n.inputfile {\n    position: absolute;\n    left: 0;\n    right: 0;\n    display: none;\n}\n\n.mat-icon {\n    height: 100%;\n    width: 100%;\n}\n\n.mat-button {\n    display: table-cell;\n    padding: 0;\n    margin: 0;\n    width: 10vh;\n    height: 10vh;\n    min-width: 0;\n    line-height: normal;\n}\n\n.mat-button span {\n    margin: 0;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}\n\n.label {\n    width: 100%;\n    font-size: 4vh;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/points/points.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<input #fileInput type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" (change)=\"loadPoints($event);\"/>\n<div class=\"container\">\n    <button mat-button><label for=\"file\" (click)=\"inputClick()\"><mat-icon svgIcon=\"button_load_points\"></mat-icon></label></button>\n    <button mat-button (click)=\"deletePoints($event);\"><mat-icon svgIcon=\"button_del_points\"></mat-icon></button>\n    <div class=\"list\">\n        <div class=\"listitem mat-caption\" *ngFor=\"let point of points$ | async | dictValues\">\n            <button mat-button (click)=\"pointSelected(point)\" style=\"grid-row: span 2\"><mat-icon svgIcon=\"button_left\"></mat-icon></button>\n            <div style=\"grid-column: span 3\">{{point.name}}</div>\n            <div>{{\"x: \"}}{{formatValue(point.x)}}</div>\n            <div>{{\"y: \"}}{{formatValue(point.y)}}</div>\n            <div>{{\"z: \"}}{{formatValue(point.z)}}</div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/points/points.component.theme.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/points/points.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PointsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__points_actions__ = __webpack_require__("../../../../../src/app/points/points.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/




var PointsComponent = /** @class */ (function () {
    function PointsComponent(pointsActions) {
        this.pointsActions = pointsActions;
        this.numberFormat = new Intl.NumberFormat('en', {
            style: 'decimal',
            maximumFractionDigits: 2,
            minimumFractionDigits: 2
        });
    }
    PointsComponent.prototype.loadPoints = function (event) {
        var files = event.target.files;
        this.pointsActions.loadPointsClick(files);
    };
    PointsComponent.prototype.deletePoints = function (event) {
        this.pointsActions.deletePointsClick();
    };
    PointsComponent.prototype.pointSelected = function (point) {
        this.pointsActions.pointSelectedClick(point);
    };
    PointsComponent.prototype.inputClick = function () {
        this.fileInput.nativeElement.value = null;
    };
    PointsComponent.prototype.formatValue = function (value) {
        return this.numberFormat.format(value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["select"])(['points']),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], PointsComponent.prototype, "points$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('fileInput'),
        __metadata("design:type", Object)
    ], PointsComponent.prototype, "fileInput", void 0);
    PointsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-points',
            providers: [__WEBPACK_IMPORTED_MODULE_3__points_actions__["d" /* PointsActions */]],
            styles: [__webpack_require__("../../../../../src/app/points/points.component.css"), __webpack_require__("../../../../../src/app/points/points.component.theme.scss")],
            template: __webpack_require__("../../../../../src/app/points/points.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__points_actions__["d" /* PointsActions */]])
    ], PointsComponent);
    return PointsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/points/points.reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return INITIAL_POINTS_STATE; });
/* harmony export (immutable) */ __webpack_exports__["b"] = pointsReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__points_actions__ = __webpack_require__("../../../../../src/app/points/points.actions.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/

var INITIAL_POINTS_STATE = {};
function pointsReducer(state, action) {
    if (state === void 0) { state = INITIAL_POINTS_STATE; }
    var stateCopy = __assign({}, state);
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__points_actions__["e" /* REINITIALIZE_POINTS */]:
            return action.points;
        case __WEBPACK_IMPORTED_MODULE_0__points_actions__["b" /* LOAD_POINTS */]:
            stateCopy = action.points;
            return stateCopy;
        case __WEBPACK_IMPORTED_MODULE_0__points_actions__["a" /* DELETE_POINTS */]:
            stateCopy = Object.assign({}, state);
            stateCopy = {};
            return stateCopy;
    }
    return state;
}


/***/ }),

/***/ "../../../../../src/app/shared/socket.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ABS_POS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SocketActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var ABS_POS = 'ABS_POS';
var SocketActions = /** @class */ (function () {
    function SocketActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    SocketActions.prototype.absPosition = function (absPosition) {
        if (this.ngRedux.getState().axes.xAxis.configuration.invert) {
            absPosition.data.X = -absPosition.data.X;
        }
        if (this.ngRedux.getState().axes.yAxis.configuration.invert) {
            absPosition.data.Y = -absPosition.data.Y;
        }
        if (this.ngRedux.getState().axes.zAxis.configuration.invert) {
            absPosition.data.Z = -absPosition.data.Z;
        }
        this.ngRedux.dispatch({ type: ABS_POS, absPosition: absPosition });
    };
    SocketActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], SocketActions);
    return SocketActions;
}());



/***/ }),

/***/ "../../../../../src/app/shared/socket.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store__ = __webpack_require__("../../../../@angular-redux/store/lib/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_redux_store___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client__ = __webpack_require__("../../../../socket.io-client/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__axis_axis_actions__ = __webpack_require__("../../../../../src/app/axis/axis.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__calculator_calculator_actions__ = __webpack_require__("../../../../../src/app/calculator/calculator.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__configuration_configuration_actions__ = __webpack_require__("../../../../../src/app/configuration/configuration.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/






var SocketService = /** @class */ (function () {
    function SocketService(ngRedux) {
        this.ngRedux = ngRedux;
        this.host = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port;
        this.connected = false;
    }
    SocketService.prototype.get = function (actions) {
        var _this = this;
        var socketUrl = this.host;
        this.socketActions = actions;
        this.socket = __WEBPACK_IMPORTED_MODULE_2_socket_io_client___default.a.connect(socketUrl, { transports: ['websocket'] });
        this.socket.on('connect', function () { return _this.connected = true; });
        this.socket.on('disconnect', function () { return _this.connected = false; });
        this.socket.on('error', function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
        this.socket.on('absPos', function (data) { actions.absPosition({ data: data }); });
        this.socket.on('loadConfiguration', function (data) {
            var axesConfig = data['axes'];
            var calculatorConfig = data['calculator'];
            var configurationConfig = data['configuration'];
            _this.ngRedux.dispatch({ type: __WEBPACK_IMPORTED_MODULE_3__axis_axis_actions__["e" /* REINITIALIZE_AXES */], axes: axesConfig });
            _this.ngRedux.dispatch({ type: __WEBPACK_IMPORTED_MODULE_4__calculator_calculator_actions__["f" /* REINITIALIZE_CALCULATOR */], calculator: calculatorConfig });
            _this.ngRedux.dispatch({ type: __WEBPACK_IMPORTED_MODULE_5__configuration_configuration_actions__["a" /* REINITIALIZE_CONFIGURATION */], configuration: configurationConfig });
        });
        return this;
    };
    SocketService.prototype.setZero = function (axisLabel) {
        if (this.connected) {
            this.socket.emit('setZero', { axis: axisLabel });
        }
        else {
            var curAbsPos = {
                data: {
                    'X': this.ngRedux.getState().axes.xAxis.absValue,
                    'Y': this.ngRedux.getState().axes.yAxis.absValue,
                    'Z': this.ngRedux.getState().axes.zAxis.absValue,
                    'magX': 0,
                    'magY': 0,
                    'magZ': 0
                }
            };
            curAbsPos.data[axisLabel] = 0;
            this.socketActions.absPosition(curAbsPos);
        }
    };
    SocketService.prototype.setAbsPostion = function (axisLabel, absPos) {
        if (axisLabel === this.ngRedux.getState().axes.xAxis.metadata.label && this.ngRedux.getState().axes.xAxis.configuration.invert) {
            absPos = -absPos;
        }
        else if (axisLabel === this.ngRedux.getState().axes.yAxis.metadata.label && this.ngRedux.getState().axes.yAxis.configuration.invert) {
            absPos = -absPos;
        }
        else if (axisLabel === this.ngRedux.getState().axes.zAxis.metadata.label && this.ngRedux.getState().axes.zAxis.configuration.invert) {
            absPos = -absPos;
        }
        if (this.connected) {
            this.socket.emit('setAbsPosition', { axis: axisLabel, absPos: absPos });
        }
        else {
            var curAbsPos = {
                data: {
                    'X': this.ngRedux.getState().axes.xAxis.absValue,
                    'Y': this.ngRedux.getState().axes.yAxis.absValue,
                    'Z': this.ngRedux.getState().axes.zAxis.absValue,
                    'magX': 0,
                    'magY': 0,
                    'magZ': 0
                }
            };
            curAbsPos.data[axisLabel] = absPos;
            this.socketActions.absPosition(curAbsPos);
        }
    };
    SocketService.prototype.saveConfiguration = function (appState) {
        if (this.connected) {
            this.socket.emit('saveConfiguration', appState);
        }
    };
    SocketService.prototype.loadConfiguration = function () {
        if (this.connected) {
            this.socket.emit('loadConfiguration');
        }
    };
    SocketService.prototype.shutdown = function () {
        if (this.connected) {
            this.socket.emit('shutdown');
        }
    };
    SocketService.prototype.reboot = function () {
        if (this.connected) {
            this.socket.emit('reboot');
        }
    };
    SocketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_redux_store__["NgRedux"]])
    ], SocketService);
    return SocketService;
}());



/***/ }),

/***/ "../../../../../src/app/shutdown/shutdown.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShutdownActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_socket_service__ = __webpack_require__("../../../../../src/app/shared/socket.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var ShutdownActions = /** @class */ (function () {
    function ShutdownActions(socketService) {
        this.socketService = socketService;
    }
    ShutdownActions.prototype.shutdownClick = function () {
        this.socketService.shutdown();
    };
    ShutdownActions.prototype.rebootClick = function () {
        this.socketService.reboot();
    };
    ShutdownActions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_socket_service__["a" /* SocketService */]])
    ], ShutdownActions);
    return ShutdownActions;
}());



/***/ }),

/***/ "../../../../../src/app/shutdown/shutdown.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n*/\n.container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 20% 40%;\n        grid-template-columns: 20% 40%;\n    -ms-grid-rows: 45% 45%;\n        grid-template-rows: 45% 45%;\n    -webkit-box-pack: space-evenly;\n        -ms-flex-pack: space-evenly;\n            justify-content: space-evenly;\n    -ms-flex-line-pack: space-evenly;\n        align-content: space-evenly;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    margin: 1vh;\n    height: 100%;\n}\n.mat-icon {\n    height: 100%;\n    width: 100%;\n}\n\n.mat-button {\n    display: table-cell;\n    padding: 0;\n    margin: 0;\n    width: 10vh;\n    height: 10vh;\n    min-width: 0;\n    line-height: normal;\n}\n\n.mat-button span {\n    margin: 0;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shutdown/shutdown.component.html":
/***/ (function(module, exports) {

module.exports = "<!--\n    DRO WebApp\n\n    Copyright (C) 2017 David Schmelter\n\n    This program is free software: you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation, either version 3 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License\n    along with this program.  If not, see\n    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or\n    <http://www.gnu.org/licenses/>.\n-->\n<div class=\"container\">\n  <button mat-button (click)=\"shutdownClick($event)\"><mat-icon svgIcon=\"button_shutdown\"></mat-icon></button>\n  <h1 class=\"mat-h1\">Shutdown</h1>\n  <button mat-button (click)=\"rebootClick($event)\"><mat-icon svgIcon=\"button_shutdown\"></mat-icon></button>\n  <h1 class=\"mat-h1\">Reboot</h1>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shutdown/shutdown.component.theme.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shutdown/shutdown.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShutdownComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shutdown_actions__ = __webpack_require__("../../../../../src/app/shutdown/shutdown.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
    DRO WebApp

    Copyright (C) 2017 David Schmelter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <https://github.com/schmelly/DRO/tree/master/dro-webapp> or
    <http://www.gnu.org/licenses/>.
*/


var ShutdownComponent = /** @class */ (function () {
    function ShutdownComponent(shutdownActions) {
        this.shutdownActions = shutdownActions;
    }
    ShutdownComponent.prototype.shutdownClick = function (event) {
        this.shutdownActions.shutdownClick();
    };
    ShutdownComponent.prototype.rebootClick = function (event) {
        this.shutdownActions.rebootClick();
    };
    ShutdownComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-shutdown',
            providers: [__WEBPACK_IMPORTED_MODULE_1__shutdown_actions__["a" /* ShutdownActions */]],
            styles: [__webpack_require__("../../../../../src/app/shutdown/shutdown.component.css"), __webpack_require__("../../../../../src/app/shutdown/shutdown.component.theme.scss")],
            template: __webpack_require__("../../../../../src/app/shutdown/shutdown.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shutdown_actions__["a" /* ShutdownActions */]])
    ], ShutdownComponent);
    return ShutdownComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");





if (__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map