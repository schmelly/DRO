FROM hypriot/rpi-alpine

WORKDIR /app

COPY dro-webapp/dro_server.conf ./
COPY dro-webapp/dro_server.py ./
COPY dro-webapp/LICENSE ./
COPY dro-webapp/requirements.txt ./
COPY dro-webapp/static/dist/ ./static/dist/

RUN    apk upgrade --no-cache \ 
    && apk add --no-cache build-base python3 python3-dev \
    && pip3 install --no-cache-dir --upgrade pip \ 
    && pip3 install --no-cache-dir -r requirements.txt \ 
    && rm -rf /var/cache/* \ 
    && rm -rf /root/.cache/*

CMD [ "python3", "dro_server.py" ]
