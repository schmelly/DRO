#!/bin/bash

# Step 1: install Docker
echo "Installing Docker"
curl -sSL https://get.docker.com | sh

# Step 2: remove existing DRO Docker image
echo "Removing previous DRO image (if exists)"
sudo docker image rm --force registry.gitlab.com/schmelly/dro/armhf/dro:master

# Step 3: pull current image
echo "Pulling new DRO image"
sudo docker pull registry.gitlab.com/schmelly/dro/armhf/dro:master

# Step 4: start the new image
echo "Starting DRO image"
sudo docker run \
    --name dro \
    --detach \
    --restart=always \
    --cap-add SYS_RAWIO \
    --device /dev/mem \
    -p 80:80 \
    registry.gitlab.com/schmelly/dro/armhf/dro:master
